#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 解析schema提供者提供的view-schema信息，把他解析为对应的View实例
@author:zhoutishun
@file:domain-svc.py
@time:2022/09/20
"""

from csrd_api_framework.framework.test.tms_view import TurnoutCreateView, TurnoutCreateChannelView


class TurnoutSvc(object):
    @classmethod
    def create_turnout(cls, turnout_info):
        turnout_create_view = TurnoutCreateView()  # 导航到创建视图
        turnout_create_view.render(turnout_info)  # 渲染（填充）数据
        turnout_channel_view = TurnoutCreateChannelView()  ## 导航或出发到通道视图
        turnout_channel_view.render(turnout_info)  # 渲染（填充）通道视图
        turnout_create_view.submit()  # 提交创建视图


class BoxSvc(object):
    def create_box(self, box_info):
        pass
