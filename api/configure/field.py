#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:  api-converter中文件字段的处理
@author:zhoutishun
@file:filed.py
@time:2021/09/08
"""


class ConverterFiledConfigure(object):
    API_FILE_FIELD = "api_file_upload"  # 文件上传字段转换标识
