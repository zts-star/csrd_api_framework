#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:  api-converter中文件字段的处理
@author:zhoutishun
@file:fileconverter.py
@time:2021/09/08
"""

from csrd_api_tms.csrd_api_framework.framework.api.converter import ApiFieldConvertContext
from csrd_api_tms.csrd_api_framework.framework.api.configure.field import ConverterFiledConfigure


@ApiFieldConvertContext.registry_converter(ConverterFiledConfigure.API_FILE_FIELD)
class FileFieldConverter(object):

    def convert(self, cmd, api):
        cmd_dict = cmd
        return cmd_dict
