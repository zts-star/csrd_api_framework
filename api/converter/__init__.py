#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:  api-converter预处理相关
@author:zhoutishun
@file:__init__.py.py
@time:2021/09/08
"""
from csrd_api_tms.csrd_api_framework.framework.api.schema.api_schema import HttpDataObject
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
import json
from copy import deepcopy
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode
from csrd_api_tms.csrd_api_framework.framework.manager.framework_context import FrameworkContext
# from csrd_api_tms.csrd_api_framework.framework.api.converter.fileconverter import FileFieldConverter


class DefaultDomainConverter(object):

    def convert(self, cmd, api):
        cmd_dict = cmd.toDict()
        logger.info("default....cmd...convert....dict...", cmd_dict)
        http_data_object = HttpDataObject(do=cmd_dict, api=api)
        return http_data_object


class ConvertPreHandler(object):
    """
    convert的预处理
    """

    @classmethod
    def handle(cls, cmd, api, converter):
        cmd_dict = cmd.toDict()
        interface_detail_schema = api.schema
        req_params = interface_detail_schema.req_params
        req_query = interface_detail_schema.req_query
        req_params_list = [req_param["name"] for req_param in req_params]
        # 2021.11.06新增req_query字典，由于查询参数列表缺失导致无法转换，固把req_query加入req_params_list中
        req_query_list = [query["name"] for query in req_query]
        req_params_list = req_params_list + req_query_list
        # end

        # 2021.12.10 新增req_body_type验证，如果是form表单则req_query_list追加req_body_form进去
        # 不然只会匹配body_other与req_query、req_params中的，会提示找不到转换字段的异常
        if interface_detail_schema.req_body_type == "form":
            req_form_list = [form_param["name"] for form_param in interface_detail_schema.req_body_form]
            req_params_list = req_params_list + req_form_list
        # end
        req_body_other = interface_detail_schema.req_body_other

        # # 2021 12.10 配套req_body_type验证
        # if interface_detail_schema.req_body_type == "json":

        if req_body_other is None or req_body_other == '':
            properties = {}
        else:
            schema_json = json.loads(req_body_other)
            logger.debug("req body other....{}".format(req_body_other))
            properties = schema_json.get("properties")
        logger.info(
            "params...{},body_other..{},converter..{},cmd_dict..{}...req_query..{}".format(req_params_list, properties,
                                                                                           converter,
                                                                                           cmd_dict, req_query))
        is_convert = False
        # 新增convert_field机制 ，之前是每个cmd都对应一个converter#
        converter_fields = []
        for cmd_key in cmd_dict:
            # if cmd_key not in req_params_list and len(req_params_list) > 0:  ###2021 1109由于req_params_list为空列表时无法找到需要转换的字段这是一个BUG，去掉长度判断
            if cmd_key not in req_params_list:
                logger.debug("cmd key in req_params_list..{}..{}".format(cmd_key, req_params_list))
                is_convert = True
                #     converter_fields.append(cmd_key)  # 需要转换的字段加入转换器字段列表中
                # if cmd_key not in properties and properties != {}:
                #     logger.info("cmd key in properties..{}..{}".format(cmd_key, properties))
                #     is_convert = True
                #     converter_fields.append(cmd_key)  # 需要转换的字段加入转换器字段列表中
                if cmd_key not in properties and properties != {}:
                    logger.info("cmd key in properties..{}..{}".format(cmd_key, properties))
                    is_convert = True
                    converter_fields.append(cmd_key)  # 需要转换的字段加入转换器字段列表中
                if properties == {}:
                    converter_fields.append(cmd_key)

        converters = cls._get_converter(converter_fields)
        return converters
        # if is_convert is True and converter is None:
        #     raise Exception("参数不匹配，请加入转换器")
        # if is_convert is True and converter is not None:
        #     return converter
        # if is_convert is False:
        #     return DefaultDomainConverter()

    @classmethod
    def _get_converter(cls, convert_fields):
        """
        通过转换字段获取本次请求所有需要的转换器
        :param convert_fields:
        :return:
        """

        logger.info("convert....fields....{}".format(convert_fields))

        converters = []
        if len(convert_fields) == 0:
            converters.append(DefaultDomainConverter())
        for field in convert_fields:
            converters.append(ApiFieldConverterFactory.get_converter(field))
        if len(convert_fields) >= 0 and len(converters) == 0:
            raise FrameworkException(FrameworkExCode.API_FIELD_CONVERTER_NOT_FOUND)

        return converters

    @classmethod
    def convert(cls, converters, cmd, api):
        """
        获取本次api，cmd所有的converters并执行转换
        :param converters:
        :param cmd:
        :param api:
        :return:
        """
        logger.debug("converters....{}".format(converters))
        cmd_dict = cmd.toDict()
        if len(converters) == 1:
            if isinstance(converters[0], DefaultDomainConverter):
                return DefaultDomainConverter().convert(cmd, api)
        http_data_object = HttpDataObject(do=cmd_dict, api=api)
        for converter in converters:
            cmd_dict = converter.convert(cmd_dict, api)

        # 这里需要把cmd_dict中需要转换的字段pop掉，因为这是接口不需要的
        for converter in converters:
            field = ApiFieldConvertContext.get_field(converter)
            if field in cmd_dict:  # 需要判断是否在cmd中，因为可能有传入参数但是转换为{}
                cmd_dict.pop(field)
        # 重新给http_data_object中的input_data赋值，因为每个需要转换的字段依次会重新赋值给cmd_dict，且完成后会pop
        http_data_object.input_data = cmd_dict
        return http_data_object

    # return wrapper


class ApiFieldConverterFactory(object):
    """
    converter工厂
    """

    @classmethod
    def get_converter(cls, src):
        return ApiFieldConvertContext.get_converter(src)
        # converter_dict = {src: ApiFieldConvertContext.get_converter(src)}
        # return converter_dict


class ApiFieldConvertContext(object):
    """
    converter上下问管理-注册、获取converter
    """
    field_converters = {}

    @classmethod
    def registry_converter(cls, field):
        """
        注册转换器
        :param field:   需要转换的字段
        :return:
        """
        logger.info("api field registry field:{}".format(field))

        def wrapper(converter_cls):
            if field not in cls.field_converters:
                cls.field_converters[field] = converter_cls()
                framework_context = deepcopy(cls.field_converters)
                FrameworkContext.api_field_context = framework_context  # 每个子模块的上下文都要向manager里面的context注册
            else:
                raise FrameworkException(FrameworkExCode.API_FIELD_CONVERTER_DUPLICATE)
            return converter_cls()

        return wrapper

    @classmethod
    def get_converter(cls, field):
        """
        通过字段查找转换器
        :param field: 注册的转换字段
        :return:
        """
        converter = cls.field_converters.get(field)
        if converter is None:
            raise FrameworkException(FrameworkExCode.API_FIELD_CONVERTER_NOT_FOUND, data=field)
        return converter

    @classmethod
    def get_field(cls, converter):
        for field, _converter in cls.field_converters.items():
            if converter is _converter:
                logger.info("found the field.....{}".format(field))
                return field


if __name__ == '__main__':
    pass
