#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:base_type.py
@time:2021/08/13
"""


class ListType(list):
    def __init__(self, ele_type=None):
        self.ele_type = ele_type
        # super(ListType, self).__init__()

    def append(self, obj):
        if self.ele_type is not None:
            if isinstance(obj, self.ele_type):
                super(ListType, self).append(obj)
            else:
                raise Exception("element type is not matched")
        else:
            super(ListType, self).append(obj)


if __name__ == '__main__':
    l = ListType()
    l.append(None)
    logger.info(l)
    logger.info (isinstance(l, list))
