#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema_provider.py
@time:2021/08/13
"""

from csrd_api_tms.csrd_api_framework.framework.api.schema.schema_config import SchemaConfig
from csrd_api_tms.csrd_api_framework.framework.helper.http_util import HttpUtil
from csrd_api_tms.csrd_api_framework.framework.api.schema.schema_provider.yapi.yapi_model import YapiInterfaceSchema, YapiInterfaceInfo
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode


class YapiClient(object):

    def __init__(self):
        logger.info("实例化实例化...")
        self.auth = None
        self.project = None
        self.payload = None
        self.params = None
        self.host = None
        self.page_limit = 1000
        self.page_first = 1
        self._get_meta_from_config()
        self.proj_schema = None
        self.proj_menus = None

    def _get_meta_from_config(self):

        self.auth = SchemaConfig.get("token")
        self.project = SchemaConfig.get("project")
        self.host = SchemaConfig.get("yapi_host")
        self.payload = {"token": self.auth}
        self.params = {"token": self.auth, "page": self.page_first, "limit": self.page_limit}
        self.detail_params = {"token": self.auth, "id": None}
        self.header = {"Content-Type": "application/json; charset=utf-8"}

    def get_proj_schemas(self):

        if self.proj_schema is None:
            res = HttpUtil.get(self.host + SchemaConfig.get("interface_list"), params=self.params,
                               headers=self.header).json()
            self.proj_schema = res["data"]["list"]
        return self.proj_schema

    def get_proj_menus(self):

        if self.proj_menus is None:
            res = HttpUtil.get(self.host + SchemaConfig.get("project_menus"), params=self.params,
                               headers=self.header).json()
            self.proj_menus = res["data"]
        return self.proj_menus
        # return res["data"]

    def get_menu_by_id(self, id):

        menues = self.get_proj_menus()
        for menu in menues:
            if menu["_id"] == id:
                return menu

    def _get_proj_schemas(self):
        schemas = self.get_proj_schemas()
        schema_objects = []
        logger.debug("schemas length....{}".format(len(schemas)))
        for schema in schemas:
            interface_info = YapiInterfaceInfo()
            interface_info.fromJson(schema)
            menu_info = self.get_menu_by_id(interface_info.catid)
            interface_info.catname = menu_info["name"]
            schema_objects.append(interface_info)
        return schema_objects

    def get_proj_interface_info(self, interface) -> YapiInterfaceInfo:
        schemas = self._get_proj_schemas()
        for schema in schemas:
            finder = InterfaceFinderFactory.get_interface_finder(schema, interface)
            found_schema = finder.found(schema, interface)
            if found_schema is not None:
                return found_schema
        raise FrameworkException(FrameworkExCode.YAPI_API_SCHEMA_NOT_FOUND, interface.title)

    def get_interface_detail(self, interface_info: YapiInterfaceInfo) -> YapiInterfaceSchema:
        self.detail_params["id"] = interface_info._id
        res = HttpUtil.get(self.host + SchemaConfig.get("interface_detail"), params=self.detail_params,
                           headers=self.header).json()
        # logger.info(res)
        interface_detail_schema = YapiInterfaceSchema()
        interface_detail_schema.fromJson(res["data"])
        return interface_detail_schema

    def set_meta_data(self, auth, project):
        self.auth = auth
        self.project = project


class InterfaceFinder(object):
    def __init__(self, schema, interface):
        self.schema = schema
        self.interface = interface

    def found(self, schema, interface):
        schema = schema.toKeyValue()
        matched = True
        return_interface_info = YapiInterfaceInfo()
        interface_input_data = interface.toKeyValue()
        for schema_key, schema_value in interface_input_data.items():
            if schema_value is not None:
                if schema_value != schema[schema_key]:
                    matched = False
        if matched is True:
            return_interface_info.fromJson(schema)
            return return_interface_info


class InterfaceFinderFactory(object):
    @classmethod
    def get_interface_finder(cls, schema, interface):
        return InterfaceFinder(schema, interface)


class YapiMetaData(object):

    def __init__(self):
        self.auth = None
        self.project = None


class YapiSchemaProvider(object):
    """
    从Yapi中提供标准的schema定义(协议)

    """

    def __init__(self):
        self.client = YapiClient()

    def get_src(self):
        pass

    def convertor(self):
        pass

    def provide(self, info):
        logger.info("#############yapischem#############{}".format(info))
        interface_info = self.client.get_proj_interface_info(info)
        detail_schema = self.client.get_interface_detail(interface_info)
        return detail_schema


if __name__ == '__main__':
    pass
