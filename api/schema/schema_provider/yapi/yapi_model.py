#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:yapi_model.py
@time:2021/08/13
"""

from csrd_api_tms.csrd_api_framework.framework.api.schema.schema_provider.base_type import ListType
from csrd_api_tms.csrd_api_framework.framework.helper.data_util import jsonModel
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode
import json


class ReqQuery(object):
    required = None
    name = None
    example = None
    desc = None


@jsonModel()
class YapiInterfaceSchema(object):
    """
    当req_body_type为json的时候properties必须有值  待验证--未开发
    """

    def __init__(self):
        self.add_time = None
        self.api_opened = None
        self.catid = None
        self.desc = None
        self.edit_uid = None
        self.index = None
        self.markdown = None
        self.method = ""
        self.path = {}
        self.project_id = None
        self.query_path = {}
        self.req_body_form = []
        self.req_body_is_json_schema = None
        self.req_body_other = None
        self.req_body_type = None
        self.req_headers = []
        self.req_params = []
        self.req_query = []
        self.res_body = ""
        self.res_body_is_json_schema = True
        self.res_body_type = "json"
        self.status = "undone"
        self.tag = []
        self.title = ""
        self.type = "static"
        self.uid = None
        self.up_time = None
        self.username = None
        self._converted_http_model_headers = {}

    def __getattribute__(self, item):
        """
        如果API定义中请求体是json，则会去验证req_body_other的格式
        :param item:
        :return:
        """
        # logger.info("getattribute..req_headers ..{}".format(object.__getattribute__(self, "req_headers")))
        if item == "req_body_other":
            if object.__getattribute__(self, "req_body_is_json_schema") is True:
                # logger.info("original req body other....{}".format(object.__getattribute__(self, item)))
                req_body_other = object.__getattribute__(self, item)
                # logger.info("original req body other....{},length..{}".format(req_body_other,len(req_body_other)))
                logger.info("xx..{}".format(req_body_other is not None and req_body_other != ''))
                if req_body_other is not None and req_body_other != '':
                    req_body_other_json = json.loads(req_body_other)
                    if req_body_other_json.get("properties") is None:
                        raise FrameworkException(FrameworkExCode.YAPI_REQUEST_BODY_SCHEMA_ERROR)
        return object.__getattribute__(self, item)

    def __convert_http_model_headers(self):
        http_model_header = {}
        # logger.info("jjjjjjjjjjjjjjjjjjjjjjjaaaa")
        for header in self.req_headers:
            # logger.info("jjjjjjjjjjjjjjjjjjjjjjj")
            header_key = header.get("name")
            header_value = header.get("value")
            http_model_header[header_key] = header_value
        return http_model_header

    @property
    def converted_http_model_headers(self):
        http_model_headers = self.__convert_http_model_headers()
        http_model_headers.update(self._converted_http_model_headers)
        return http_model_headers

    def add_convert_http_model_headers(self, header: dict):
        # logger.info("adddddddddd.....header...{}".format(header))
        self._converted_http_model_headers.update(header)


@jsonModel()
class YapiInterfaceInfo(object):
    def __init__(self):
        self.add_time = None
        self.api_opened = None
        self.catid = None
        self.edit_uid = None
        self.method = ""
        self.project_id = None
        self.path = ""
        self.status = "undone"
        self.tag = []
        self.title = ""
        self.uid = None
        self._id = None
        self.catname = None


import json

jstr = "{\"up_time\": \"123\"}"
a = json.loads(jstr)
y = YapiInterfaceSchema()
y.fromJson(a)
logger.info(y)
logger.info(y.toKeyValue())


class YapiResBody(object):
    pass


class YapiBaseType(object):
    base_type = None


if __name__ == '__main__':
    from jsonschema import validate

    from jsonschema import validate, SchemaError, ValidationError, draft7_format_checker

    js = {'$schema': "http://json-schema.org/draft-04/schema#", "type": "object",
          "properties": {"status": {"type": "string"}, "data": {"type": "array", "items": {"type": "object",
                                                                                           "properties": {"radarId": {
                                                                                               "type": "string"},
                                                                                               "radarNo": {
                                                                                                   "type": "string"}}}}},
          "required": ["status"]}
    # 以上是定义的schema的校验规则，具体实际业务中可自行更改校验规则
    # 以下为待校验的响应数据
    response = {
        "name": "s",
        "money": 2000,
        "tags": [],
        "time": "22",
        "token": "000038efc7edc7438d781b0775eeaa009cb64865",
        "tt": "dd",
        "otherinfo1": {
            "otherinfo1": "789",
            "otherinfo2": "123"
        },

        "status": "1",
        "data": []
    }
    # 以下为校验代码
    try:
        # 校验, 跟assert断言一个意思
        # validate 校验成功时候，不会有报错
        # JSON 数据校验失败，抛出 jsonschema.exceptions.ValidationError 异常
        # schema 模式本身有问题，抛出 jsonschema.exceptions.SchemaError 异常
        validate(instance=response, schema=js)
    except SchemaError as e:
        logger.info("验证模式schema出错")
    except ValidationError as e:
        logger.info("json数据不符合schema规定：提示信息：" + e.message)
    else:
        logger.info("校验数据正确！")
