#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema_reader.py
@time:2021/08/12
"""

from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class SchemaReader(object):
    """
    从标准的schema协议中读取内容：目前是从provider中读取对应的schema
    """

    def __init__(self):
        pass

    def read(self, schema):
        return schema


if __name__ == '__main__':
    pass
