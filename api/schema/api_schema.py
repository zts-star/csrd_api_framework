#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:api_schema.py
@time:2021/08/12
"""

# from csrd_api_tms.csrd_api_framework.framework.data.builder.dto_converter import ApiDtoConvert
from csrd_api_tms.csrd_api_framework.framework.api.schema.schema_provider.yapi.yapi_provider import YapiSchemaProvider
from csrd_api_tms.csrd_api_framework.framework.api.schema.schema_reader import SchemaReader
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.helper.data_util import jsonModel


class ApiSchema(object):
    def __init__(self, src, api):
        self.src = src
        self.api_name = api
        self.api = InterfaceInputData(api)
        # self.schema = self._get_schema(api)
        self.schema = self._get_schema(self.api)
        self.input_data = None
        self.data_instance = None

    def _get_provider(self):
        if self.src == "yapi":
            return YapiSchemaProvider()

    def _get_schema(self, api):
        logger.debug("api....schema....{}".format(api))
        schema_provider = self._get_provider()
        reader = SchemaReader()
        schema = reader.read(schema_provider.provide(api))
        logger.debug("_get schema......{}".format(schema))
        # logger.info("api....schema....", api, schema.__dict__)
        return schema


class HttpSchema(object):
    def __init__(self):
        self.uri = None
        self.body = None
        self.params = None
        self.header = None
        self.address = None
        self.protocol = None
        self.resp = None
        self.method = None


class HttpDataObject(object):
    def __init__(self, do, api):
        self.input_data = do
        self.api = api


@jsonModel()
class InterfaceInputData(object):
    def __init__(self, interface):
        self.title = None
        self.catname = None
        self.method = None
        self._convert(interface)

    def _convert(self, interface):
        interface_list = interface.split("|")
        if len(interface_list) > 0:
            self.title = interface_list[0]
            self.catname = self._get_item(interface_list, 1)
            self.method = self._get_item(interface_list, 2)

    def _get_item(self, src, index):
        try:
            item = src[index]
            return item
        except IndexError:
            return None
