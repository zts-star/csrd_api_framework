#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema_config.py
@time:2021/08/12
"""

from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework import frame_config

SchemaConfig = frame_config.config.get("api_meta")

if __name__ == '__main__':
    pass
