#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:emiter.py
@time:2021/08/31
"""
from csrd_api_tms.csrd_api_framework.framework.api.converter import DefaultDomainConverter, ConvertPreHandler
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class Emiter(object):
    @classmethod
    def emit(cls, api=None, converter=None, sender=None, cmd=None):
        """

        :param api:  api定义信息
        :param converter:  输入数据转换器
        :param sender:      使用哪个引擎
        :param cmd:     输入数据
        :return:
        """
        # convert = ConvertPreHandler.handle(cmd, api, converter)
        # logger.info("handler converter...{}".format(convert))
        converters = ConvertPreHandler.handle(cmd, api, converter)
        http_obj = ConvertPreHandler.convert(converters, cmd, api)
        # if converter is None:
        #     converter = DefaultDomainConverter()

        # http_obj = convert.convert(cmd, api)
        result = sender.send(http_obj)
        return result


if __name__ == '__main__':
    pass
