#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:sender.py
@time:2021/08/31
"""
from csrd_api_tms.csrd_api_framework.framework.data.builder.build_api_data import ApiEmiterDataBuilder
from csrd_api_tms.csrd_api_framework.framework.data.builder.build_api_resp_data import ApiRespDataBuilder
import requests
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.data.model.api_context import GlobalApiContext
import json


class ApiSender(object):
    """
    引擎
    """

    def __init__(self, dest):
        self.dest = dest

    def send(self, http_data_obj):
        builder = ApiEmiterDataBuilder()
        resp_builder = ApiRespDataBuilder()
        api_model_data = builder.build(http_data_obj)
        api_model_data = self._assembler(api_model_data)
        res = RequestTools().send(api_model_data)
        logger.info("#####################http响应#####################\r\n{}".format(res.text))
        res = resp_builder.build(resp=res)
        # logger.info("resp content is..{}".format(res.__to_json__()))
        GlobalApiContext.set_api_req_model(api_model_data)
        GlobalApiContext.set_api_resp_model(res)

        return res

    def _assembler(self, model_data):
        model_data.protocol = self.dest.protocol
        model_data.host = self.dest.host
        model_data.port = self.dest.port
        model_data.postfix = self.dest.postfix
        return model_data


class RequestTools(object):
    session = requests.Session()

    # logger.info("session....." + str(session))

    def __init__(self):
        pass

    def send(self, model):
        # logger.info("session....." + str(self.session))
        logger.debug("sender model...{}".format(model))
        headers = getattr(model, "header")
        # print("headers1111..." + str(headers))
        method = getattr(model, "method")
        uri = getattr(model, "uri")
        body = getattr(model, "body")
        port = getattr(model, "port")
        host = getattr(model, "host")
        postfix = getattr(model, "postfix")
        protocol = getattr(model, "protocol")
        query = getattr(model, "query")
        file=getattr(model,'file')
        logger.info(
            "query_data...{query}.....req_body_type:{req_body_type}".format(query=query,
                                                                            req_body_type=model.schema.req_body_type))
        url = protocol + "://" + host + ":" + port + postfix

        # data = {"url": "http://192.168.1.121:3000/mock/65" + uri, "headers": headers, "json": body}
        # 2021.12.10新增如果是form表单形式则新增data字段，在传给requests的字段中新增data，去除json
        if model.schema.req_body_type == 'form':
            data = {"url": url + uri, "headers": headers, "data": body, "params": query}
        else:
            data = {"url": url + uri, "headers": headers, "json": body, "params": query}
        # data = {"url": "http://10.168.2.204:9080" + uri, "headers": headers, "json": body}

        method_func = getattr(self.session, method)
        logger.info("#####################request-data#####################：\r\n{}".format(
            json.dumps(data, sort_keys=True, indent=2)))
        res = method_func(**data)
        logger.info("RequestTools res text..\r\n{}".format(res.text))

        return res


if __name__ == '__main__':
    pass
