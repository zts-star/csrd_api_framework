# csrd_api_framework

#### 介绍
不同于pytest、unittest等，定位于测试应用框架，支持api与webui，提出数据集概念，统一使用数据驱动API与WEBUI。支持数据服务、API管理、视图管理。

#### 框架流程
![输入图片说明](https://foruda.gitee.com/images/1673337073178364254/ea6a8709_9775669.png "屏幕截图")




#### 使用说明

**API管理器：**
    - API定义解析器：
    	API定义与管理与测试业务分离，在YAPI中定义好API，解析器负责解析为内部协议。
    - API数据组装：
    	 把业务数据组装为定义好的schema，因为业务数据通常只是输入一些常用字段，API定义中需要其他许多的字段则可按照定义自动组装。  

**数据集组件（服务）：**
     - 数据集生成：
         根据设计好的模板，生成测试用例需要的指定数据集。
     - 数据设计器：
         设计数据集，格式需要设计我们的数据定义，其数据来源可以来自excel、csv、数据库等。
     - 数据验证器：
         根据根据定义好的数据格式（schema），通过验证器自动验证数据有效性。  



#### 样例代码  

**上层-业务API，供三方标准框架调用。**
  
```python

    class UserApi(object):
        @BaseValidatorInjector.inject(ApiValidator.expect_api_success)
        @without_params_data_provider
        def add_user(self, dataset):
            resp = UserApp().add_user(dataset)
            return resp

```


**业务领域层--业务逻辑-接口：**
  
 ```python

    class RoleService(BaseApiService):
        """	领域服务	"""
    
        def add_role(self, cmd):
            return self.send(cmd, SchemaInfo.lms_role_add_role_schema)
    
        def del_role(self, cmd):
            return self.send(cmd, SchemaInfo.lms_role_del_role_schema)
 ```

**业务领域层-业务逻辑-WEB-UI：**  


```python
class SysRoleCreateView(metaclass=ViewMetaClass):
    """	视图实体	"""
    def __init__(self):
        pass

    def setup(self, view_obj):
        pass

class RoleOpService(object):
    """	领域服务	"""
    @classmethod
    def create_role(cls, cmd):
        role_create_view = SysRoleCreateView()
        role_create_view.render(cmd)
        role_create_view.submit()
```  


**数据设计**
  
  |  project   | module  |  func   |      desc       |  dataset_type   | dataset_name  |  dataset_params   | dataset_values  |dataset_ext|
  | ------ | ------ | ------ |------ |---------------- |------ |------ |------ |------ |
  | 测试项目  | 认证 |登录功能 |【功能验证-正常】可以正常登录 |业务数据集 |valid.loginuser |username,password |admin,#[md5('admin')] |resp.success |










