#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:framework_context.py
@time:2021/11/15
"""


class FrameworkContext(object):
    api_field_context = {}
    dataset_field_context = {}

    @classmethod
    def get_context(cls):
        return {"api_field_context": cls.api_field_context, "dataset_field_context": cls.dataset_field_context}

    @classmethod
    def get_api_field_context(cls):
        return cls.api_field_context

    @classmethod
    def get_dataset_field_context(cls):
        return cls.dataset_field_context


if __name__ == '__main__':
    pass
