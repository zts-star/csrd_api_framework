#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:sidecar.py
@time:2022/10/20
"""
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import ElementNotInteractableException
from csrd_api_framework.framework.e2e.prd.webui.manager.webui_manager import WebUiManager
import time
from csrd_api_framework.framework.e2e.prd.webui.element.command import CommandTypes
from csrd_api_framework.framework.e2e.prd.webui.element.command import CommandManager


class ElementTyeCommandSideCar(object):
    @classmethod
    def command(cls, command_type):
        pass

    @classmethod
    def define(cls, ele_type):
        def ele_type_class_wrapper(ele_type_side_car_cls):
            CommandManager.add_ele_type(ele_type, ele_type_side_car_cls)
            return ele_type_side_car_cls

        return ele_type_class_wrapper


class BaseElementTypeCommand(object):
    def __init__(self, element):
        self.element = element

    def click(self):
        raise NotImplementedError

    def get_text(self, args=None):
        raise NotImplementedError

    def set_value(self, args=None):
        raise NotImplementedError

    def clear(self):
        raise NotImplementedError

###外部调用command的边车


# @sidecar
