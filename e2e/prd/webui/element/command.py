#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema.py
@time:2022/09/26
"""
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import ElementNotInteractableException, ElementClickInterceptedException
from csrd_api_framework.framework.e2e.prd.webui.manager.webui_manager import WebUiManager
import time


class CommandManager(object):
    enable_set_ele_types = ['input', 'select', 'special_input']
    ele_types = {}

    def add_command(self):
        pass

    def add_set_ele_type(self):
        pass

    @classmethod
    def add_ele_type(cls, ele_type, side_car):
        if ele_type not in ele_type:
            raise Exception('ele type is exist...{}'.format(ele_type))
        else:
            cls.ele_types.setdefault(ele_type, side_car)

    @classmethod
    def get_ele_type_side_car(cls, ele_type):
        return cls.ele_types.get(ele_type)


class CommandTypes(object):
    CLICK = 'click'
    GET_TEXT = 'get_text'
    SET_VALUE = 'set_value'
    CLEAR = 'clear'


class _ElementCommand(object):
    @classmethod
    def execute(cls, element):
        pass

    @classmethod
    def get_side_car(cls, element):
        ele_type = element.schema.type
        ele_type_side_car = CommandManager.get_ele_type_side_car(ele_type)
        print("ele_type.....{}....ele_type_side_car...{}".format(ele_type, ele_type_side_car))
        if ele_type_side_car is None:
            return ele_type_side_car
        ele_type_side_car_obj = ele_type_side_car(element)
        print("side car.....{}".format(ele_type_side_car_obj))
        return ele_type_side_car_obj


class _ElementCLickCommand(_ElementCommand):
    @classmethod
    def execute(cls, element, args=None):
        sidecar = cls.get_side_car(element)
        if sidecar is None:
            ##如果这个元素类型没有指定的边车，则调用默认的方法。否则调用该类型指定的click方法
            cls.common_click(element, args)
        else:
            try:
                sidecar.click()
            except NotImplementedError as e:
                ###如果没有实现，但是仍然调用则调用默认的点击方法
                print(e)
                cls.common_click(element, args)
        #
        # element.init()
        #
        # print("真的点击了")
        # print(element.base_element)
        # print(element.schema.locator_value)
        # time.sleep(1)
        # element.base_element.click()

    @classmethod
    def common_click(cls, element, args=None):
        element.init()

        print("真的点击了")
        print(element.base_element)
        print(element.schema.locator_value)
        time.sleep(1)
        element.base_element.click()


class _ElementClearCommand(_ElementCommand):
    @classmethod
    def execute(cls, element):
        pass


class _ElementSetValueCommand(_ElementCommand):
    # @classmethod
    # def execute(cls, element, args=None):
    #     """
    #     内置几种element的set_value方法，如input
    #     :param element:
    #     :param args:
    #     :return:
    #     """
    #
    #     ele_type = element.schema.type
    #     if ele_type not in CommandManager.enable_set_ele_types:
    #         raise Exception(" element type [{}] not be set value ".format(ele_type))
    #     else:
    #         element.init()
    #
    #         if element.schema.type == 'special_input':
    #             print("xxxxxx special input...{}".format(element.base_element))
    #             print(element.base_element.get_attribute("id"))
    #             driver = WebUiManager.get_driver()
    #             action = ActionChains(driver)
    #             # time.sleep(2)
    #             element.base_element.send_keys('')
    #             print("argsss.....{}".format(args))
    #             action.move_to_element(element.base_element).send_keys(args).perform()
    #         else:
    #             print("xxxxxx common input...{}".format(element.base_element))
    #             print(element.base_element.get_attribute("id"))
    #             element.base_element.send_keys(args)

    @classmethod
    def execute(cls, element, args=None):
        """
        内置几种element的set_value方法，如input
        :param element:
        :param args:
        :return:
        """

        ele_type = element.schema.type
        if ele_type not in CommandManager.enable_set_ele_types:
            raise Exception(" element type [{}] not be set value ".format(ele_type))
        else:
            element.init()
            ele_type_side_car_obj = cls.get_side_car(element)
            ele_type_side_car_obj.set_value(args)

    @classmethod
    def execute1(cls, element, args=None):
        ele_type_side_car_obj = cls.get_side_car(element)
        ele_type_side_car_obj.set_value(args)


class _ElementGetTextCommand(_ElementCommand):
    # @classmethod
    # def execute(cls, element, args=None):
    #     # ele_type = element.schema.type
    #     element.init()
    #     element.inits()
    #     print(element.schema.type)
    #     if element.schema.type == 'input':
    #         print("wowc")
    #         print(dir(element.base_element))
    #         print(element.base_element.get_attribute('outerHTML'))
    #         print(element.base_element.get_attribute('value'))
    #         return element.base_element.get_attribute('value')
    #     if element.schema.type == 'dropdown_items':
    #         print("drop...woc")
    #         elements = element.base_elements
    #         item_values = []
    #         for element in elements:
    #             print("diao.....")
    #             item_value = element.get_attribute('innerText')
    #             item_values.append(item_value)  ##### 2022.10.19新增下拉列表文本获取表达
    #         return item_values
    @classmethod
    def execute(cls, element, args=None):
        # ele_type = element.schema.type
        element.init()
        element.inits()
        print(element.schema.type)
        ele_type_side_car_obj = cls.get_side_car(element)
        print("ele_type_side_car_obj.....{}".format(ele_type_side_car_obj))
        return_value = ele_type_side_car_obj.get_text(args)
        return return_value


class Command(object):
    CLICK = _ElementCLickCommand
    SET_VALUE = _ElementSetValueCommand
    CLEAR = _ElementClearCommand
    GET_TEXT = _ElementGetTextCommand


class CommandBuilder(object):
    def build(self):
        pass


class CommandExecutor(object):
    def __init__(self, element):
        self.element = element

    def execute(self, command, args=None):
        count = 0
        click_try_count = 0
        while True:
            try:
                if count > 30:
                    raise Exception("over {} times  that element still be interactable..".format(count))
                if click_try_count == 30:
                    raise Exception("over {} times  that element still be not clickable..".format(click_try_count))
                ele_command = command
                result = ele_command.execute(self.element, args)
                print("return result....{}".format(result))
                return result
                # break
            except ElementNotInteractableException as e:
                count = count + 1
                time.sleep(1)
            except ElementClickInterceptedException as e:
                click_try_count = click_try_count + 1
                time.sleep(1)
