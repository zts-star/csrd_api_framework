#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema.py
@time:2022/09/26
"""
from selenium.webdriver.common.by import By
from csrd_api_framework.framework.e2e.prd.webui.manager.webui_manager import WebUiManager
from csrd_api_framework.helper.common_helper import logger
import time


class ElementFinder(object):

    @classmethod
    def get_element(cls, schema, args):
        element = None
        if schema.locator_type == 'xpath':
            element = cls._find_element(By.XPATH, schema.locator_value)
        if schema.locator_type == 'name':
            element = cls._find_element(By.NAME, schema.locator_value)
        if schema.locator_type == 'id':
            element = cls._find_element(By.ID, schema.locator_value)
        return element

    @classmethod
    def get_element_from_element(cls, ele, schema):
        element = None
        if schema.locator_type == 'xpath':
            element = cls._find_element_from_element(ele, By.XPATH, schema.locator_value)
        if schema.locator_type == 'name':
            element = cls._find_element_from_element(ele, By.NAME, schema.locator_value)
        if schema.locator_type == 'id':
            element = cls._find_element_from_element(ele, By.ID, schema.locator_value)
        return element

    @classmethod
    def get_elements_from_element(cls, ele, schema):
        element = None
        if schema.locator_type == 'xpath':
            element = cls._find_elements_from_element(ele, By.XPATH, schema.locator_value)
        if schema.locator_type == 'name':
            element = cls._find_elements_from_element(ele, By.NAME, schema.locator_value)
        if schema.locator_type == 'id':
            element = cls._find_elements_from_element(ele, By.ID, schema.locator_value)
        return element

    @classmethod
    def get_elements(cls, schema, args):
        elements = []
        if schema.locator_type == 'xpath':
            elements = cls._find_elements(By.XPATH, schema.locator_value)
        if schema.locator_type == 'name':
            elements = cls._find_elements(By.NAME, schema.locator_value)
        if schema.locator_type == 'id':
            elements = cls._find_elements(By.ID, schema.locator_value)
        return elements

    @classmethod
    def _find_element(cls, by_type, value):
        count = 0
        while True:
            if count > 30:
                raise Exception("over {} times still find element {}".format(count, value))
            try:
                driver = WebUiManager.get_driver()
                element = driver.find_element(by_type, value)
                return element
            except Exception as e:
                print(e)
                time.sleep(1)
                count = count + 1

    @classmethod
    def _find_element_from_element(cls, element, by_type, value):
        count = 0
        while True:
            # if count > 30:
            if count > WebUiManager.get_find_fail_count():
                raise Exception("over {} times still find element {}".format(count, value))
            try:
                element = element.find_element(by_type, value)
                return element
            except Exception as e:
                print(e)
                time.sleep(1)
                count = count + 1

    @classmethod
    def _find_elements_from_element(cls, _element, by_type, value):
        count = 0
        empty_elements_count = 0
        elements = []
        while True:
            logger.info(empty_elements_count)
            # if count > 30:
            if count > WebUiManager.get_finds_fail_count():
                raise Exception("over {} times still find element {}".format(count, value))
            print(empty_elements_count)
            # if empty_elements_count > 30:
            if empty_elements_count > WebUiManager.get_finds_fail_count():
                return elements

            try:
                elements = _element.find_elements(by_type, value)  ###当元素找不到的时候，find_elements不会提示异常，而直接返回空列表
                logger.info(elements)
                logger.info(elements == [])
                if elements == []:
                    print("return empty elements")
                    empty_elements_count = empty_elements_count + 1
                else:
                    return elements
            except Exception as e:
                print(e)
                time.sleep(1)
                count = count + 1

    @classmethod
    def _find_elements(cls, by_type, value):
        count = 0
        empty_elements_count = 0
        elements = []
        while True:
            logger.info(empty_elements_count)
            if count > 30:
                raise Exception("over {} times still find element {}".format(count, value))
            print(empty_elements_count)
            if empty_elements_count > 30:
                return elements

            try:
                driver = WebUiManager.get_driver()
                elements = driver.find_elements(by_type, value)  ###当元素找不到的时候，find_elements不会提示异常，而直接返回空列表
                logger.info(elements)
                logger.info(elements == [])
                if elements == []:
                    print("return empty elements")
                    empty_elements_count = empty_elements_count + 1
                else:
                    return elements
            except Exception as e:
                print(e)
                time.sleep(1)
                count = count + 1

    # element.click()
    # return element
