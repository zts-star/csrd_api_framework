#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema.py
@time:2022/09/26
"""
import sys

sys.path.append(r'c:\Users\zhoutishun\PycharmProjects\csrd_api_tms')
from csrd_api_framework.framework.helper.data_util import jsonModel


@jsonModel()
class ResourceSchema(object):
    def __init__(self):
        self.view_name = None
        self.name = None
        self.type = None
        self.alias = None
        self.locator_type = None
        self.locator_value = None
        self.dataset_ext = None
        self.dec = None
        self.component_name = None
        self.is_result = None
        self.is_render = None

    def parameterize(self, *args):
        """
        参数化目前只针对locator_value
        :param args:
        :return:
        """
        print("parameterize....{}".format(args))
        format_locator_value = self.locator_value.format(*args)
        self.locator_value = format_locator_value

    def is_rendered(self):
        if self.is_render == 'N':
            return False
        else:
            return True

    def is_result(self):
        if self.is_result == 'N':
            return False
        else:
            return True


class ResourceType(object):
    SUBMIT = 'submit'
    INPUT = 'input'
    SELECT = 'select'
    CLOSE = 'close'


b = {'alias': '线路类型输入', 'component_name': 'line_type_component', 'dataset_ext': '数据集是不会管API怎么设计的，是完全无依赖的,耦合的',
     'dec': '线路类型输入', 'func': '创建线路', 'locator_type': 'xpath', 'locator_value': '//*/span[text() = "{}"]/../..',
     'module': '基础数据', 'name': 'line_type_input', 'project': 'pub', 'submodule': '线路管理', 'type': 'input',
     'view_name': 'N/A'}
r = ResourceSchema()
r.fromJson(b)
print(r.locator_value)
