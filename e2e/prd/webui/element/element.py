#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema.py
@time:2022/09/26
"""
from csrd_api_framework.framework.e2e.prd.webui.element.helper import ElementFinder
from csrd_api_framework.framework.e2e.prd.webui.element.command import CommandExecutor, Command


class Element(object):
    def __init__(self, schema):
        self.schema = schema
        self.base_element = None
        self.base_elements = []

    def get_text(self, args=None):
        text = self.__execute(Command.GET_TEXT, args)
        return text

    def click(self, args=None):
        self.__execute(Command.CLICK, args)

    def clear(self):
        self.__execute(Command.CLEAR)

    def set_value(self, value):
        self.__execute(Command.SET_VALUE, value)

    def init(self, args=None):
        print("init  argss;;; {}".format(args))
        print("after...{}".format(self.schema.locator_value))
        self.base_element = ElementFinder.get_element(self.schema, args)

    def inits(self, args=None):
        print('get elements...{}'.format(self.schema.locator_value))
        self.base_elements = ElementFinder.get_elements(self.schema, args)
        print("base elements....{}".format(self.base_elements))

    def find_from_self(self, schema):
        """
        新增从元素下继续寻找元素
        :param schema:
        :return:
        """
        return ElementFinder.get_element_from_element(self.base_element, schema)

    def __execute(self, cmd, args):
        executor = CommandExecutor(self)
        result = executor.execute(cmd, args)
        return result
