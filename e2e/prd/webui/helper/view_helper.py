#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 视图助手
@author:zhoutishun
@file:event_resolver.py
@time:2022/10/08
"""
from csrd_api_framework.framework.e2e.prd.webui.manager.webui_manager import WebUiManager
import time


class ViewHelper(object):
    @classmethod
    def route(cls, url):
        driver = WebUiManager.get_driver()
        driver.get(url)
        # time.sleep(30)
