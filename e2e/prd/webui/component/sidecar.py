#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:sidecar.py
@time:2022/10/20
"""

from csrd_api_framework.framework.e2e.prd.webui.component.command import ComponentCommandManager


class ComponentTyeCommandSideCar(object):
    @classmethod
    def command(cls, command_type):
        pass

    @classmethod
    def define(cls, component_types):
        def component_type_class_wrapper(component_type_side_car_cls):
            ComponentCommandManager.add_component_type(component_types, component_type_side_car_cls)
            return component_type_side_car_cls

        return component_type_class_wrapper


class BaseComponentTypeCommand(object):
    def __init__(self, component):
        self.component = component

    def set_value(self, args=None):
        raise NotImplementedError

    def get_text(self, args=None):
        raise NotImplementedError

###外部调用command的边车


# @sidecar
