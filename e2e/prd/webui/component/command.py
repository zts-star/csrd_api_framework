#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema.py
@time:2022/09/26
"""
from selenium.webdriver.common.action_chains import ActionChains
from csrd_api_framework.framework.e2e.prd.webui.manager.webui_manager import WebUiManager
from csrd_api_framework.framework.e2e.prd.webui.element.element import Element
import time


class ComponentCommandManager1(object):
    enable_set_component_ele_types = ['select_component']

    def add_command(self):
        pass

    def add_set_ele_type(self):
        pass


class ComponentCommandManager(object):
    enable_set_component_ele_types = ['select_component', 'tree_node_component', 'check_group']
    component_types = {}

    def add_command(self):
        pass

    def add_set_ele_type(self):
        pass

    @classmethod
    def add_component_type(cls, component_type, side_car):
        if component_type in cls.component_types:
            raise Exception('ele type is exist...{}'.format(component_type))
        else:
            cls.component_types.setdefault(component_type, side_car)

    @classmethod
    def get_component_type_side_car(cls, component_type):
        return cls.component_types.get(component_type)


class _ComponentCommand(object):
    @classmethod
    def execute(cls, component):
        pass

    @classmethod
    def get_side_car(cls, component):
        component_type = component.schema.component_type
        component_type_side_car = ComponentCommandManager.get_component_type_side_car(component_type)
        print("component_type.....{}....ele_type_side_car...{}".format(component_type, component_type_side_car))
        if component_type_side_car is None:
            return component_type_side_car
        ele_type_side_car_obj = component_type_side_car(component)
        print("side car.....{}".format(ele_type_side_car_obj))
        return ele_type_side_car_obj


class _ComponentElementSetValueCommand(_ComponentCommand):
    # @classmethod
    # def execute(cls, component_element, args=None):
    #     """
    #     component得set_value命令，根据不同类型其set_value的操作不一样
    #     :param component_element: 整个组件元素
    #     :param args:
    #     :return:
    #     """
    #
    #     ele_type = component_element.schema.component_type
    #     if ele_type not in ComponentCommandManager.enable_set_component_ele_types:
    #         raise Exception(" component element type [{}] not be set value ".format(ele_type))
    #     else:
    #         component_element.init()
    #         if component_element.schema.component_type == 'select_component':
    #             print("开始渲染 select component。。。。。")
    #             component_elements = component_element.component_resources_elements
    #             print("需要渲染的组件元素。。。{}".format(component_elements))
    #             for element in component_elements:
    #                 print(element.schema.type)
    #                 element.init()
    #                 print("select component组件元素开始赋值")
    #                 element.click()

    @classmethod
    def execute(cls, component_element, args=None):
        """
        component得set_value命令，根据不同类型其set_value的操作不一样
        :param component_element: 整个组件元素
        :param args:
        :return:
        """

        ele_type = component_element.schema.component_type
        if ele_type not in ComponentCommandManager.enable_set_component_ele_types:
            raise Exception(" component element type [{}] not be set value ".format(ele_type))
        else:
            component_element.init()
            component_type_side_car_obj = cls.get_side_car(component_element)
            component_type_side_car_obj.set_value(args)


class _ComponentElementGetTextCommand(_ComponentCommand):
    # @classmethod
    # def execute(cls, component_element, args=None):
    #     """
    #     component得set_value命令，根据不同类型其set_value的操作不一样
    #     :param component_element: 整个组件元素
    #     :param args:
    #     :return:
    #     """
    #
    #     ele_type = component_element.schema.component_type
    #     if ele_type not in ComponentCommandManager.enable_set_component_ele_types:
    #         raise Exception(" component element type [{}] not be set value ".format(ele_type))
    #     else:
    #         component_element.init()
    #         if component_element.schema.component_type == 'select_component':
    #             print("开始渲染 select component。。。。。")
    #             component_elements = component_element.component_resources_elements
    #             print("需要渲染的组件元素。。。{}".format(component_elements))
    #             for element in component_elements:
    #                 print(element.schema.type)
    #                 element.init()
    #                 print("select component组件元素开始赋值")
    #                 element.click()

    @classmethod
    def execute(cls, component_element, args=None):
        """
        component得set_value命令，根据不同类型其set_value的操作不一样
        :param component_element: 整个组件元素
        :param args:
        :return:
        """

        ele_type = component_element.schema.component_type
        # if ele_type not in ComponentCommandManager.enable_set_component_ele_types:
        #     raise Exception(" component element type [{}] not be set value ".format(ele_type))
        # # else:
        #     component_element.init()
        #     component_type_side_car_obj = cls.get_side_car(component_element)
        #     component_type_side_car_obj.set_value(args)

        component_element.init()
        component_type_side_car_obj = cls.get_side_car(component_element)
        result = component_type_side_car_obj.get_text(args)
        return result


class ComponentCommand(object):
    SET_VALUE = _ComponentElementSetValueCommand
    GET_TEXT = _ComponentElementGetTextCommand


class ComponentCommandBuilder(object):
    def build(self):
        pass


class ComponentCommandExecutor(object):
    def __init__(self, element):
        self.element = element

    def execute(self, command, args=None):
        component_command = command
        result = component_command.execute(self.element, args)
        return result
