#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 视图的渲染器
@author:zhoutishun
@file:comele.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.element.element import Element
from csrd_api_framework.framework.e2e.prd.webui.component.command import ComponentCommandExecutor, ComponentCommand


class ComEle(object):
    """
    元素有组件元素与控件元素，组件元素渲染的set_value与控件元素的set_value不一致
    """

    def __init__(self, schema):
        print("初始化组件元素....{}".format(schema))
        self.schema = schema
        self.component_resources_elements = []

    def init(self):
        print("组件初始化...{}".format(self.schema.resources))
        for resource in self.schema.resources:
            print("看看")
            resource_element = Element(resource)
            self.component_resources_elements.append(resource_element)

    def set_value(self, value):
        self.__execute(ComponentCommand.SET_VALUE, value)

    def get_text(self, args=None):
        return self.__execute(ComponentCommand.GET_TEXT, args)

    def __execute(self, cmd, args):
        executor = ComponentCommandExecutor(self)
        result = executor.execute(cmd, args)
        return result
