#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 视图的渲染器
@author:zhoutishun
@file:schema.py
@time:2022/09/20
"""

from csrd_api_framework.framework.helper.data_util import jsonModel


@jsonModel()
class ComponentSchema(object):
    def __init__(self):
        self.views = None
        self.component_name = None
        self.component_type = None
        self.dataset_ext = None
        self.resources = []

    def parameterize(self, *args):
        """
        参数化目前只针对locator_value
        :param args:
        :return:
        """
        for resource in self.resources:
            print("parameterize....{}".format(args))
            format_locator_value = resource.locator_value.format(*args)
            resource.locator_value = format_locator_value
