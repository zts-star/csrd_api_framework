#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 解析schema提供者提供的view-schema信息，把他解析为对应的View实例
@author:zhoutishun
@file:view_action.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.event.view_event import ViewSubmitEvent, ViewNavigationEvent, \
    ViewInitTriggerEvent, ViewInitCloseEvent


class ViewAction(object):
    """
    通过解析出来的view，找到其中的submit类型，然后提交
    """

    @classmethod
    def submit(cls, view_mapper):
        submit_event = ViewSubmitEvent(view_mapper)
        submit_event.handle()

    @classmethod
    def navigate(cls, view_mapper):
        navigate_event = ViewNavigationEvent(view_mapper)
        navigate_event.handle()

    @classmethod
    def init_trigger(cls, view_mapper):
        init_trigger_event = ViewInitTriggerEvent(view_mapper)
        init_trigger_event.handle()

    @classmethod
    def get_content(cls, view_mapper):
        pass

    @classmethod
    def close(cls, view_mapper):
        close_event = ViewInitCloseEvent(view_mapper)
        close_event.handle()
