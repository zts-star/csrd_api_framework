#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 视图的渲染器
@author:zhoutishun
@file:render.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.element.element import Element
from csrd_api_framework.framework.e2e.prd.webui.component.comele import ComEle


class ViewRender(object):
    """
    当数据源中的view配置被解析为view实例后，视图的渲染就需要把view实例和渲染的输入数据进行映射匹配，找到相同的字段，然后赋值等

    """

    def __init__(self, view_mapper=None):
        self.view_mapper = view_mapper

    def render(self, view_mapper=None):
        input_map = self.view_mapper.view_mapper_entity.map  # 这个映射key是input_entity_key_value，value是element实例
        for input_entity, resource in input_map.items():

            if isinstance(resource, Element):  #### 2022.10.13添加组件####
                value = input_entity.input_item_value
                resource.set_value(value)  ###2022.10.08 不需要传mapper到set_value里面进去..这是底层不需要知道mapper。只需要知道自己要什么即可，不管上层
            #### 2022.10.13添加组件####
            if isinstance(resource, ComEle):
                com_value = input_entity.input_item_value
                resource.set_value(com_value)
            ###### 2022.10.13
