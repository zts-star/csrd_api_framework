#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 解析schema提供者提供的view-schema信息，把他解析为对应的View实例
@author:zhoutishun
@file:view_behavior.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.action.view_render import ViewRender
from csrd_api_framework.framework.e2e.prd.webui.view.mapper import ViewMapper
from csrd_api_framework.framework.e2e.prd.webui.action.view_action import ViewAction
from csrd_api_framework.framework.e2e.prd.webui.manager.view_manager import ViewManager
from csrd_api_framework.framework.data.builder.command_buider import CommandOrigin


class ViewBehavior(object):

    def __init__(self, view_origin_obj):
        # self.elements = None
        # self.subviews = None
        # self.components = None
        # self.trigger = None
        # self.navigation = None

        ### 初始化的时候是直接根据
        self.view_origin_obj = view_origin_obj
        self.view_mapper = ViewMapper(view_origin_obj)  # 这里通过被装饰的view，进行映射与配置好解析出来的一样映射

        print("init.....")

    def render(self, _input):
        """
        映射起来的mapper，把他给render。render会自动填充渲染
        :return:
        """
        if isinstance(_input, CommandOrigin):
            _input = _input.get_command()
        else:
            _input = _input
        self.view_mapper.map(_input)  # 映射起view与input
        ViewRender(self.view_mapper).render()

    def submit(self):
        """
        调用action
        :return:
        """
        ViewAction.submit(self.view_mapper)

    def init(self):
        # ViewAction.navigate(self.view_mapper.get_navigation())
        ViewAction.navigate(self.view_mapper)
        import time
        # time.sleep(10)
        ViewAction.init_trigger(self.view_mapper)

    def init_setup(self):
        """
        视图初始化前的预置处理
        :return:
        """

        "初始化时候的逻辑：当view初始化的时候需要校验档期那current_view是否是sub_view且存在close事件"
        self.__close_current_sub_view()
        ViewManager.set_current_view(self.view_origin_obj)

    @classmethod
    def __close_current_sub_view(cls):
        current_view = ViewManager.get_current_view()
        print("当前视图： {}".format(current_view))
        if current_view is not None:
            current_view_mapper = ViewMapper(current_view)
            relation = current_view_mapper.parser.parser_view_relation()
            print("每次初始化前检测是否有包含close事件的sub_view存在当前视图中。。。关系表 {}".format(relation))
            print("开始比较.....{}.....{}")
            print(current_view_mapper.parser.view_src_obj_view_name)
            if relation[
                -1] != current_view_mapper.parser.view_src_obj_view_name:  # 如果view关系列表中第一个元素不是current_view本身，那么则代表current_view是sub_view
                close_resource = current_view_mapper.get_close()

                if close_resource is not None:
                    print("找到关闭资源!!!!!!!!!")
                    ViewAction.close(current_view_mapper)
            ###### 如果没有subview，那么这个view存在close得话，也应该调用close方法。因为要离开这个view，进入其他view  2022.11.01
            else:
                close_resource = current_view_mapper.get_close()

                if close_resource is not None:
                    print("找到关闭资源!!!!!!!!!")
                    ViewAction.close(current_view_mapper)

            ######
