#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: view得行为触发哪些view事件，事件接收事件参数，做出事件处理
@author:zhoutishun
@file:event_resolver.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.view.mapper import ViewMapper
from csrd_api_framework.framework.e2e.prd.webui.view.parser import ViewSchemaParser
from csrd_api_framework.framework.e2e.prd.webui.element.element import Element
import time


class EventViewMapperExpressOpNode(object):
    """
    目前去掉to_view类型，to_ele与click是主要得节点类型。WEB页面来说，基本就只会有一个主界面，一个main_view。所有得都是从这个main_view中进行跳转
    比如tms就是从menu_tree视图开始，可以跳转到任何一个页面
    """
    # op_node_type = ['to_ele', 'to_view', 'click']
    op_node_type = ['to_ele', 'click']

    @classmethod
    def to_ele(cls, resource, *args):
        print("ele....args...{resource}...{args}".format(resource=resource, args=args))
        parser = ViewSchemaParser()
        resource_schema = parser.parser_resource_by_name(resource)
        resource_schema.parameterize(*args)
        ele = Element(resource_schema)
        ele.click()

    def to_view(self, view):
        pass

    @classmethod
    def click(cls, resource):
        parser = ViewSchemaParser()
        resource_schema = parser.parser_resource_by_name(resource)
        print("click....node.....")
        ele = Element(resource_schema)
        ele.click()


class EventViewMapperExpress(object):
    """
    解析表达式为节点并运行节点列表
    """

    def __init__(self, view_express_origin):
        self.view_express_origin = view_express_origin
        self.node_chain = []

    def run(self):
        for node in self.node_chain:
            node_type = node.get('node_type')
            node_ext = node.get('node_ext')
            node_express = node.get('node_express')
            if node_type == 'view':
                view_obj = node_ext
                view_obj.init()
            if node_type == 'op_inner_node':
                node_op = node.get('node_op')
                node_op_method = getattr(EventViewMapperExpressOpNode, node_op)
                node_op_method_args = self.get_op_node_args(node_express)
                # time.sleep(1)
                node_op_method(*node_op_method_args)

    def get_op_node_args(self, express):
        print("origin express....{}".format(express))
        arg_start = express.find('(')
        reverse_express = express[::-1]
        args_end = reverse_express.find(')')
        args_end = len(express) - 1 - args_end
        args = express[arg_start + 1:args_end]
        args_return = args.split(',')
        return args_return

    def append_node(self, node):
        """
        node是一个字典：格式为{"node_type":"node_express":""}
        :param node:
        :return:
        """
        self.node_chain.append(node)


class EventViewMapperResolver(object):
    """
    视图事件解析器，解析指定事件，目前有submit navigation视图事件，解析好后返回表达式，然后在事件处理器上运行该表达式
    """

    def __init__(self, event_args):
        self.event_args = event_args

    def resolve_navigation(self):
        navigation = self.event_args.get_navigation()
        express_nodes = navigation.split('.')
        mapper_express = EventViewMapperExpress(navigation)
        for index, express_item in enumerate(express_nodes):
            if index == 0 and express_item.find('(') < 0:  # 2022.10.10 如果当前节点下标为0且节点字符串不包括括号符号，则表示这个节点是view类型
                view_schema_parser = ViewSchemaParser(view_src_name=express_item)
                view_schema_obj = view_schema_parser.parser_view_by_name()
                node = {'node_type': 'view', 'node_express': express_item, 'node_ext': view_schema_obj}
                mapper_express.append_node(node)
            else:
                for node_type in EventViewMapperExpressOpNode.op_node_type:
                    if express_item.find(node_type) == 0:
                        node = {'node_type': 'op_inner_node', 'node_express': express_item, 'node_ext': None,
                                'node_op': node_type}
                        mapper_express.append_node(node)

        return mapper_express

    def resolve_init_trigger(self):
        init_trigger = self.event_args.get_init_trigger()
        express_nodes = init_trigger.split('.')
        mapper_express = EventViewMapperExpress(init_trigger)
        for express_item in express_nodes:
            # for node_type in EventViewMapperExpressOpNode.op_node_type:
            #     if express_item.find(node_type) == 0:
            #         node = {'node_type': 'op_inner_node', 'node_express': express_item, 'node_ext': None,
            #                 'node_op': node_type}
            #         mapper_express.append_node(node)
            self.__get_op_node_mapper_express(mapper_express, express_item)
        return mapper_express

    def __get_op_node_mapper_express(self, mapper_express, express_item):
        for node_type in EventViewMapperExpressOpNode.op_node_type:
            if express_item.find(node_type) == 0:
                node = {'node_type': 'op_inner_node', 'node_express': express_item, 'node_ext': None,
                        'node_op': node_type}
                mapper_express.append_node(node)

    def _get_node(self, express):
        pass
