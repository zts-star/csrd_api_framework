#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: view得行为触发哪些view事件，事件接收事件参数，做出事件处理
@author:zhoutishun
@file:view_event.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.view.mapper import ViewMapper
from csrd_api_framework.framework.e2e.prd.webui.event.event_resolver import EventViewMapperResolver
from csrd_api_framework.framework.e2e.prd.webui.helper.view_helper import ViewHelper


class ViewSubmitEvent(object):
    def __init__(self, event_args: ViewMapper):
        self.event_args = event_args

    def handle(self):
        submit_ele = self.event_args.get_submit()
        submit_ele.init()
        submit_ele.click()


class ViewNavigationEvent(object):
    def __init__(self, event_args: ViewMapper):
        self.event_args = event_args
        self.resolver = EventViewMapperResolver(self.event_args)

    def handle(self):
        # navigation = self.event_args.get_navigation()
        if self.event_args.parser.meta_viw_obj.navigation == 'N/A':
            route = self.event_args.parser.meta_viw_obj.router
            ViewHelper.route(route)  # 当导航为N/A的时候，就通过路由来进行跳转
        else:
            #####新增异常处理 2022.11.07如果导航不为N/A，那么先运行导航表达式，如果导航表达式发生异常且route不为N/A，那则跳转到route。如果route为N/A则抛出异常
            try:
                express_node = self.resolver.resolve_navigation()  # 把导航表达式解析成节点，然后运行节点
                express_node.run()
            except Exception as e:
                route = self.event_args.parser.meta_viw_obj.router
                if route == 'N/A':
                    raise e
                else:
                    ViewHelper.route(route)
            #### 2022.11.08##


class ViewInitTriggerEvent(object):
    def __init__(self, event_args: ViewMapper):
        self.event_args = event_args
        self.resolver = EventViewMapperResolver(self.event_args)

    def handle(self):
        # navigation = self.event_args.get_navigation()
        express_node = self.resolver.resolve_init_trigger()  # 把导航表达式解析成节点，然后运行节点
        print("开始click event...")
        express_node.run()


class ViewInitCloseEvent(object):
    def __init__(self, event_args: ViewMapper):
        self.event_args = event_args

    def handle(self):
        try:
            ##如果关闭事件处理出现异常，这不属于业务异常，可不用考虑
            close_ele = self.event_args.get_close()
            close_ele.init()
            close_ele.click()
        except Exception as e:
            print("[error]: {}".format(e))
            pass
