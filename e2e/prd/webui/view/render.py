#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 视图的渲染器
@author:zhoutishun
@file:render.py
@time:2022/09/20
"""


class ViewRender(object):
    """
    当数据源中的view配置被解析为view实例后，视图的渲染就需要把view实例和渲染的输入数据进行映射匹配，找到相同的字段，然后赋值等
    """

    def render(self,view_mapper):
        pass
