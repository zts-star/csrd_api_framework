#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 解析schema提供者提供的view-schema信息，把他解析为对应的View实例
@author:zhoutishun
@file:parser.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.view.provier import ViewSchemaProvider
from csrd_api_framework.framework.e2e.prd.webui.view.schema import ViewSchema
from csrd_api_framework.framework.e2e.prd.webui.element.schema import ResourceSchema
from csrd_api_framework.framework.e2e.prd.webui.component.schema import ComponentSchema
# from csrd_api_framework.framework.e2e.prd.webui.view.meta import ViewMetaClass


class SchemaReader(object):
    def __init__(self):
        self.view_schema_provider = ViewSchemaProvider()
        self.view_schema_provider.provide()
        self.view_class_field = 'view_class'
        self.view_name_field = 'view_name'
        self.resource_name_field = 'name'
        self.component_name_field = 'component_name'
        self.view_component_filed = 'components'

    def get_special_view(self, view_src_obj):
        if type(view_src_obj).__name__=='ViewMetaClass':
            view_src_obj_name = view_src_obj.__name__
        else:
            view_src_obj_name = type(view_src_obj).__name__
        print("class name...{}".format(view_src_obj_name))
        # #### 2022.10.18 对view_src_obj为类而不是对象时的处理###
        #
        # if view_src_obj_name == 'type':
        #     view_src_obj_name = view_src_obj.__name__
        # else:
        #     view_src_obj_name = view_src_obj_name

        ####
        # view_src_obj_name = view_src_obj.__name__

        meta_views = self.view_schema_provider.get_views()
        print("wcccccccccccccccccccc!!!.....{}".format(meta_views))
        for view_meta_item in meta_views:
            view_class = view_meta_item.get(self.view_class_field)
            if view_src_obj_name == view_class:
                return view_meta_item

    def get_special_component_by_name(self, component_name):
        meta_components = self.view_schema_provider.get_components()
        print("meta_components....111 {}".format(meta_components))
        for meta_component_item in meta_components:
            _component_name = meta_component_item.get(self.component_name_field)
            if _component_name == component_name:
                return meta_component_item

    def get_component_resource(self, component_name):
        meta_resources = self.view_schema_provider.get_resources()
        component_resource = []
        for meta_resource_item in meta_resources:
            print(meta_resource_item)
            resource_component_name = meta_resource_item.get(self.component_name_field)
            # print("get component resource...{}".format(resource_component_name))
            if resource_component_name == component_name:
                component_resource.append(meta_resource_item)
        print("component resource list...{}".format(component_resource))
        return component_resource

    def get_special_components_by_view(self, view_name):
        meta_views = self.view_schema_provider.get_views()
        print("query view name....{}".format(view_name))
        print("meta_views......{}".format(meta_views))

        for meta_view_item in meta_views:
            meta_view_name = meta_view_item.get(self.view_name_field)
            print("get special components view....{}".format(meta_view_name))
            print("get special components view1....{}".format(meta_view_item))
            if meta_view_name == view_name:
                print("match....")
                special_view_components = meta_view_item.get(self.view_component_filed)
                if special_view_components != 'N/A':
                    _special_view_components = special_view_components.split(',')
                    return _special_view_components
                if special_view_components == 'N/A':
                    return []

    def get_special_components_content_by_view(self, view_name):
        components = self.get_special_components_by_view(view_name)
        print(" 1231312312 ....{}....{}".format(view_name, components))
        _components = []
        for component_name in components:
            component_content = self.get_special_component_by_name(component_name)
            _components.append(component_content)
        return _components

    def get_special_resource(self, view_meta_item):
        meta_view_name = view_meta_item.get('view_name')
        meta_resources = self.view_schema_provider.get_resources()
        special_meta_resources = []
        for resource in meta_resources:
            resource_view_name = resource.get(self.view_name_field)
            if meta_view_name == resource_view_name:
                special_meta_resources.append(resource)
        return special_meta_resources

    def get_special_view_by_view_name(self, view_name):
        meta_views = self.view_schema_provider.get_views()
        print("9999999999999999 {}".format(meta_views))
        print("00000000 {}".format(view_name))
        for meta_view in meta_views:
            print("wo qu...{}  {}".format(view_name, meta_view.get(self.view_name_field)))
            if meta_view.get(self.view_name_field) == view_name:
                return meta_view

    def get_special_resource_by_resource_name(self, resource_name):
        meta_resources = self.view_schema_provider.get_resources()
        for resource in meta_resources:
            if resource.get(self.resource_name_field) == resource_name:
                return resource

    def get_view_up_relation(self, view_name):

        view_relation = []
        view_relation.append(view_name)

        def recursion_get_relation(view_name_rec):
            print("!!!!!!!!!!!!!!@@@@@@@@@!!!!!!!!!!!!!!! view_relation......{}".format(view_relation))
            print("0909...{}".format(view_name_rec))
            parent_view = view_name_rec
            # view_relation.append(parent_view)
            meta_view = self.get_special_view_by_view_name(view_name_rec)
            parent_view = meta_view.get('parent_view')
            print("0909 parent view..{}".format(parent_view))

            if parent_view == 'N/A':
                # parent_view = view_name
                # view_relation.append(parent_view)
                return view_relation
            else:
                view_relation.append(parent_view)
                print("10 10 10 {}".format(view_relation))
                print("111111!!!!!!!!!!!!!!@@@@@@@@@!!!!!!!!!!!!!!! view_relation......{}".format(view_relation))
                return recursion_get_relation(parent_view)

        recursion_get_relation(view_name)
        return view_relation


class ViewSchemaParser(object):
    def __init__(self, view_src_obj=None, view_src_name=None):
        self.view_src_obj = view_src_obj
        self.meta_viw_obj = None
        self.schema_reader = SchemaReader()
        self.view_src_name = view_src_name
        print("parser ..... view_src_name...{}...view_src_obj....{}".format(self.view_src_name, view_src_obj))
        self.view_src_obj_view_name = None

    def read(self, schema):
        pass

    def parser(self):
        meta_view = self.schema_reader.get_special_view(self.view_src_obj)
        meta_view_resources = self.schema_reader.get_special_resource(meta_view)
        print("meta view......{}".format(meta_view))
        print("meta_view_resources meta_view_resources....{} ".format(meta_view_resources))
        view_schema = ViewSchema()
        meta_view_obj = view_schema.fromJson(meta_view)
        meta_view_obj = view_schema
        self.view_src_obj_view_name = meta_view_obj.view_name  # 2022.10.11当解析的时候初始化view对象对应的名称供其他使用
        ##add 2022.1013 添加组件
        view_components = self.schema_reader.get_special_components_content_by_view(
            meta_view_obj.view_name)
        print("view_components........{}".format(view_components))
        for view_component in view_components:
            print("xxxxxxxx  123....{}".format(view_component))
            component_schema_obj = ComponentSchema()
            component_schema_obj.fromJson(view_component)
            component_resources = self.parser_component_resources(component_schema_obj.component_name)
            component_schema_obj.resources = component_resources  ####把组件对应的元素shema添加进去 2022.10.13…^_^由于上一次渲染component的时候资源列表为空无法渲染
            meta_view_obj.component_objs.append(component_schema_obj)
        #########################

        for meta_view_resource in meta_view_resources:
            print("add add resource....")
            resource_schema = ResourceSchema()
            meta_resource_obj = resource_schema.fromJson(meta_view_resource)
            meta_resource_obj = resource_schema
            meta_view_obj.resources.append(meta_resource_obj)
        self.meta_viw_obj = meta_view_obj
        return meta_view_obj

    def parser_view_by_name(self):
        view_schema = ViewSchema()
        meta_view = self.schema_reader.get_special_view_by_view_name(self.view_src_name)
        if meta_view is None:
            raise Exception("meta view is not exist {}".format(self.view_src_name))
        view_schema.fromJson(meta_view)
        self.meta_viw_obj = view_schema
        return view_schema

    def parser_resource_by_name(self, resource_name):
        print("parser resource resource_name....{}".format(resource_name))
        self.schema_reader = SchemaReader()
        resource = self.schema_reader.get_special_resource_by_resource_name(resource_name)
        if resource is None:
            raise Exception("meta resource is not exist {}".format(resource_name))
        resource_schema = ResourceSchema()
        resource_schema.fromJson(resource)
        return resource_schema

    def parser_view_relation(self):

        relation = self.schema_reader.get_view_up_relation(self.view_src_obj_view_name)
        return relation

    def parser_component_resources(self, component_name):
        component_resources = []
        print("component name ...{}".format(component_name))
        resources = self.schema_reader.get_component_resource(component_name)
        for resource in resources:
            print("from json component resource...{}".format(resource))
            component_resource_schema = ResourceSchema()
            component_resource_schema.fromJson(resource)
            component_resources.append(component_resource_schema)
        return component_resources
