#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 解析schema提供者提供的view-schema信息，把他解析为对应的View实例
@author:zhoutishun
@file:mapper.py
@time:2022/09/20
"""

from csrd_api_framework.framework.e2e.prd.webui.view.parser import ViewSchemaParser
from csrd_api_framework.framework.data.model.dataset import DataSetResult
from csrd_api_framework.framework.data.builder.command_buider import CommandOrigin
from csrd_api_framework.framework.helper.data_util import jsonModel
from csrd_api_framework.framework.e2e.prd.webui.element.schema import ResourceType
from csrd_api_framework.framework.e2e.prd.webui.element.element import Element
from csrd_api_framework.framework.e2e.prd.webui.component.comele import ComEle


class ViewMapperEntity(object):
    """
     映射后生成的实体
    """

    def __init__(self):
        self.input = None
        self.view_obj = None
        self.map = {}


# @jsonModel()
class ViewMapperInputEntity(object):
    def __init__(self):
        self.input_item_name = None
        self.input_item_value = None


class ViewMapperInput(object):
    def __init__(self, _input):
        self.input = _input

    def to_input(self) -> dict:
        print("self.input...{}".format(self.input))
        if isinstance(self.input, DataSetResult):
            return self.input.dataset_params_value
        if isinstance(self.input, dict):
            return self.input
        if isinstance(self.input, CommandOrigin):
            return self.input.get_command()


class ViewMapper(object):
    def __init__(self, view_src_obj):
        self.parser = ViewSchemaParser(view_src_obj)  # 已经开始找到对应的元素了
        self.view_obj = self.parser.parser()
        self.view_mapper_entity = ViewMapperEntity()
        self.view_mapper_entity.view_obj = self.view_obj  # 2022 1009 转换实例化前置到mapper初始化
        print("view obj resource....{}".format(self.view_obj.resources))

    def get_elements(self):
        return self.view_mapper_entity.view_obj.resources

    def get_navigation(self):
        navigation = self.view_mapper_entity.view_obj.navigation
        return navigation

    def map(self, _input):
        """
        这个是把源view里面的input与配置的view映射起来
        :return:
        """
        self.view_mapper_entity.input = ViewMapperInput(_input).to_input()
        # view_obj = self.parser.parser()
        # self.view_mapper_entity.view_obj = view_obj
        for input_item_name, input_item_value in self.view_mapper_entity.input.items():
            print("view mapper input_item_name ...{}".format(input_item_name))
            # resource = self.view_obj.get_special_resource(input_item_name)  ####遍历input数据把这些数据和view里面的映射起来，待完成
            # resource_ele = Element(resource)
            ### 2022.10.13 添加组件
            resource = None
            resource_ele = None
            if self.view_obj.is_resource(input_item_name):
                resource = self.view_obj.get_special_resource(input_item_name)
                resource_ele = Element(resource)
            if self.view_obj.is_component(input_item_name):
                resource = self.view_obj.get_special_component(input_item_name)
                # 让组件里面的元素参数化。。。。
                # resource.parameterize(input_item_value) ####2022.11.05组件不在mapper里面自动参数化，让使用者在自定义类型的时候自己手动参数化
                resource_ele = ComEle(resource)
            ########2022.10.13

            if resource is None:
                print("element not be mapped {input_item_name}".format(input_item_name=input_item_name))
                # raise Exception("element not be mapped {input_item_name}".format(input_item_name=input_item_name)) ###找不到不抛异常，因为这些数据不只是为了找资源
            else:
                input_entity = ViewMapperInputEntity()
                input_entity.input_item_value = input_item_value
                input_entity.input_item_name = input_item_name
                self.view_mapper_entity.map.setdefault(input_entity, resource_ele)

    def get_submit(self):
        resources = self.get_elements()
        for resource in resources:
            if resource.type == ResourceType.SUBMIT:
                return Element(resource)

    def get_init_trigger(self):
        init_trigger = self.view_mapper_entity.view_obj.init_trigger
        return init_trigger

    def get_view_relation(self):
        pass

    def get_close(self):
        resources = self.get_elements()
        for resource in resources:
            if resource.type == ResourceType.CLOSE:
                return Element(resource)
