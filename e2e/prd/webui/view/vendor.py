#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 供货商，提供view的相机定义给provider
@author:zhoutishun
@file:vendor.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.data.provider import WebUiMetaDataProvider
from csrd_api_tms.csrd_api_framework.framework import frame_config


class ZdViewSchemaVendor(object):
    def __init__(self):
        config = {"project": "tms_ui", "address": "http://127.0.0.1:9999"}
        # config = frame_config.config.get("ui_view_data_meta")
        self.meta_data_provider = WebUiMetaDataProvider(config)

    def supply(self):
        views = self.meta_data_provider.provide_view()
        resource = self.meta_data_provider.provide_resource()
        components = self.meta_data_provider.provide_components()
        return {"views": views, "resources": resource, "components": components}


default_vendor = ZdViewSchemaVendor()
