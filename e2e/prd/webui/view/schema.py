#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:schema.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.view.render import ViewRender
from csrd_api_framework.framework.helper.data_util import jsonModel


class View(object):
    def __init__(self):
        """
        这个视图的实例化就是要从provider中去寻找对应资数据源的视图描述
        """
        self.elements = None
        self.subviews = None
        self.components = None
        self.trigger = None
        self.navigation = None

    def to(self, view):
        pass

    def render(self, input):
        view_render = ViewRender()
        view_render.render(input)

    def submit(self):
        pass


@jsonModel()
class ViewSchema(object):
    def __init__(self):
        self.view_name = None
        self.view_class = None
        self.view_type = None
        self.navigation = None
        self.init_trigger = None
        self.subview = None
        self.router = None
        self.dataset_ext = None
        self.resources = []
        self.components = []
        self.component_objs = []

    def get_special_resource(self, resource_name):
        for resource in self.resources:
            if resource.name == resource_name:
                return resource

    def get_special_component(self, component_name):
        for component in self.component_objs:
            if component.component_name == component_name:
                return component

    def is_component(self, name):
        if name in self.components:
            return True
        else:
            return False

    def is_resource(self, name):
        for resource in self.resources:
            if resource.name == name:
                return True
        return False


class A(object):
    def __init__(self):
        print("aaaaa")

    def sender(self):
        print("send")


def sender(self, input):
    pass


# def view_dec(view_class):
#     class ViewDecInnerClass(view_class, ViewDecClass):
#         def __init__(self, *args, **kwargs):
#             view_class.__init__(self)
#             ViewDecClass.__init__(self, view_class)
#
#     return ViewDecInnerClass


def view_dec1(view_class):
    class A(view_class):
        pass

    return A


class BaseView(object):
    def __init__(self):
        pass
