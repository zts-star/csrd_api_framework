#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:meta.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.manager.view_manager import ViewManager
from csrd_api_framework.framework.e2e.prd.webui.view.render import ViewRender
from csrd_api_framework.framework.e2e.prd.webui.action.view_behaviro import ViewBehavior


class ViewMetaClass(type):
    def __new__(cls, name, bases, attrs):
        print("0000000 meta class {name}-- {bases} --{attrs}".format(name=name, bases=bases, attrs=attrs))

        new_cls = type.__new__(cls, name, bases, attrs)  # 返回一个view的类
        new_cls.render = cls.render
        new_cls.submit = cls.submit
        new_cls.get_arg = cls.get_arg
        new_cls.__new__ = cls.meta_class_new
        # new_cls.setup = cls.setup
        new_cls.__name__ = name
        print(type(new_cls))
        return new_cls

    @staticmethod
    def meta_class_new(cls, *args, **kwargs):
        """
        实例化的时候就要开始去做导航之类的view初始的事情
        :param cls:
        :param args:
        :param kwargs:
        :return:
        """
        print("args:{}...kwargs:{}".format(args, kwargs))
        cls_obj_arg = {"args": args, "kwargs": kwargs}
        cls_obj = object.__new__(cls)
        cls_obj.__init_args__ = cls_obj_arg  # 2022.11.17支持view实例输入初始化数据
        print("cls_obj_arg...{}".format(cls_obj_arg))
        # ViewManager.set_current_view(cls_obj)  # 把实例化的view写入当前视图
        view_behavior = ViewBehavior(cls_obj)
        cls_obj.setup(
            view_behavior.view_mapper.view_obj)  # 2022.11.17在实例化view的__init__实例化之前会调用每个view自己的setup方法实现一些拦截的操作，传入的参数就是view_meta_obj
        view_behavior.init_setup()
        view_behavior.init()
        return cls_obj

    @staticmethod
    def render(self_view, view_input):
        view_behavior = ViewBehavior(self_view)
        view_behavior.render(view_input)

    @staticmethod
    def submit(self_view):
        view_behavior = ViewBehavior(self_view)
        view_behavior.submit()

    @staticmethod
    def get_arg(self_view):
        """
        获取view实例的初始化数据
        :param self_view:
        :return:
        """
        return self_view.__init_args__
