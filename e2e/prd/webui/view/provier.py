#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 视图的提供出入口
@author:zhoutishun
@file:provider.py
@time:2022/09/20
"""
from csrd_api_framework.framework.e2e.prd.webui.view.vendor import default_vendor


class ViewSchemaProvider(object):
    def __init__(self):
        self.vendor = default_vendor
        self.schema = None

    def provide(self):
        schema = self.vendor.supply()
        self.schema = schema

    def get_views(self):
        return self.schema.get("views")

    def get_resources(self):
        return self.schema.get("resources")

    def get_components(self):
        return self.schema.get("components")
