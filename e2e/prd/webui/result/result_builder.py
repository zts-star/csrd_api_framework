#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 解析schema提供者提供的view-schema信息，把他解析为对应的View实例
@author:zhoutishun
@file:view_result.py
@time:2022/10/18
"""
from csrd_api_framework.framework.e2e.prd.webui.view.parser import ViewSchemaParser
from csrd_api_framework.framework.e2e.prd.webui.element.element import Element
from csrd_api_framework.framework.e2e.prd.webui.component.comele import ComEle


class ViewResultBuilder(object):
    """
    构造view的结果：其实就是返回一个view里面的各个元素的值
    """

    def __init__(self, view_class):
        self.view_class = view_class
        self.parser = ViewSchemaParser(view_src_obj=view_class)
        self.meta_view_obj = self.parser.parser()

    def build(self):
        result_building = ViewResultBuilding()
        result_building.view_class = self.view_class
        result_building.view_schema = self.meta_view_obj
        view_full_resource = self._get_view_resources()
        for resource in view_full_resource:
            ###这里把组件的所有元素和非组件的元素都合并在一起，组合成了结果。比如像table组件，他里面的元素是不会单独以result返回的。所以table里面的is_result都是N。但是table_component却实现了get_text。所以调用viewresult的时候还要构建组件的结果
            print("xxxx111")
            print(resource.is_result)
            if resource.is_result == 'N':
                continue
            resource_item = ResultItemBuildFactory.build_resource_item(resource)

            result_building.view_schema_result.append(resource_item)
        #####2022.10.31不仅返回resource结果，也要返回com的结果
        components_schemas = self._get_view_components()
        for component_schema in components_schemas:
            component_item = ResultItemBuildFactory.build_component_item(component_schema)
            result_building.view_schema_result.append(component_item)
        ##### end 2022.10.31

        return result_building

    def _get_view_resources(self):
        """
        2022.10.18 构建视图结果
        :return:
        """
        view_full_resource = []
        meta_view_obj = self.meta_view_obj
        view_root_resources = meta_view_obj.resources
        view_full_resource = view_full_resource + view_root_resources
        view_components = meta_view_obj.component_objs
        for component in view_components:
            for resource in component.resources:
                view_full_resource.append(resource)
        return view_full_resource

    def _get_view_components(self):
        return self.meta_view_obj.component_objs


class ResultItemBuildFactory(object):
    @classmethod
    def build_resource_item(cls, resource):
        ele = Element(resource)
        print("ele ...get_text....{}".format(ele))
        value = ele.get_text()
        resource_item = {resource.name: value}
        return resource_item

    @classmethod
    def build_component_item(cls, component):
        com_ele = ComEle(component)
        com_name = component.component_name
        com_value = com_ele.get_text()
        print("com value....{}".format(com_value))
        component_item = {com_name: com_value}
        return component_item


class ViewResultBuilding(object):
    def __init__(self):
        self.view_class = None
        self.view_schema = None
        self.view_schema_result = []
