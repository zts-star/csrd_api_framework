#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 解析schema提供者提供的view-schema信息，把他解析为对应的View实例
@author:zhoutishun
@file:view_result.py
@time:2022/10/18
"""
from csrd_api_framework.framework.e2e.prd.webui.result.result_builder import ViewResultBuilder


class ViewResult(object):
    def __init__(self, view_class):
        self.view_class = view_class
        self.view_name = None
        self.view_schema = None
        self.view_result = None
        self._initial()

    def _initial(self):
        builder = ViewResultBuilder(self.view_class)
        result_building = builder.build()
        self.view_result = result_building.view_schema_result
        self.view_schema = result_building.view_schema
        self.view_name = self.view_schema.view_name

    def get_item(self, item):
        """
        新增按key获取结果列表中对应的项
        :param item:
        :return:
        """
        for item_value in self.view_result:
            item_value_name = list(item_value.keys())[0]
            if item_value_name == item:
                return list(item_value.values())[0]

###外部调用
# result = ViewResult(ViewClass)
