#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 解析schema提供者提供的view-schema信息，把他解析为对应的View实例
@author:zhoutishun
@file:provider.py
@time:2022/09/26
"""

from csrd_api_framework.framework.data.provider.auto import AutoDataProvider, AutoProvider


class WebUiMetaDataProvider(object):
    def __init__(self, config):
        self.config = config
        self.auto_provider = AutoProvider(self.config)
        self.auto_data_provider = AutoDataProvider(self.config)
        self.view_dataset_type = 'view'
        self.resource_dataset_type = 'resource'
        self.component_dataset_type = 'component'

    def provide_resource(self):
        resource_meta_content = self.get_data_type_instance_data(self.resource_dataset_type)
        return resource_meta_content

    def provide_view(self):
        view_meta_content = self.get_data_type_instance_data(self.view_dataset_type)
        return view_meta_content

    def provide_components(self):
        component_meta_content = self.get_data_type_instance_data(self.component_dataset_type)
        return component_meta_content

    def _load_src_dataset(self):
        dataset_src = self.auto_provider.get_project_config().get("dataset")
        dataset_src_list = dataset_src.split('|')
        return dataset_src_list

    def _get_dataset_type(self):
        dataset_src_types = self.auto_provider.get_project_config().get("dataset_type")
        data_type_list = dataset_src_types.split('|')
        return data_type_list

    def _get_dataset_type_instance_map(self):
        dataset_types = self._get_dataset_type()
        dataset_instances = self._load_src_dataset()
        print("6666666   {}".format(dataset_types))
        print("7777777   {}".format(dataset_instances))
        data_type_instance_map = {}
        for index, dataset_type in enumerate(dataset_types):
            data_instance = [dataset_instances[index]]
            if dataset_type not in data_type_instance_map:
                data_type_instance_map.setdefault(dataset_type, data_instance)
            else:
                data_type_instance_map[dataset_type].append(data_instance)
        return data_type_instance_map

    def get_data_type_instance_data(self, data_type):
        data_type_instance_map = self._get_dataset_type_instance_map()
        data_instances = data_type_instance_map.get(data_type)
        data_instance_content = []
        for data_instance in data_instances:
            content = self.auto_data_provider.load_data(data_instance)
            print("8888888888   {}".format(type(content)))
            data_instance_content = data_instance_content + content
        return data_instance_content
