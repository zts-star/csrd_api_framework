#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: view得行为触发哪些view事件，事件接收事件参数，做出事件处理
@author:zhoutishun
@file:event_resolver.py
@time:2022/09/20
"""


class ViewManager(object):
    __current_view = None

    @classmethod
    def get_current_view(cls):
        return cls.__current_view

    @classmethod
    def set_current_view(cls, view_src_obj):
        cls.__current_view = view_src_obj
