#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: view得行为触发哪些view事件，事件接收事件参数，做出事件处理
@author:zhoutishun
@file:event_resolver.py
@time:2022/09/20
"""
from selenium import webdriver


class WebUiManager(object):
    driver = None
    find_fail_count = 1
    finds_fail_count = 1

    @classmethod
    def get_driver(cls):
        if cls.driver is None:
            driver = webdriver.Chrome()
            driver.maximize_window()
            cls.driver = driver

        return cls.driver

    def set_driver(self):
        pass

    @classmethod
    def set_find_fail_count(cls, fail_count):
        cls.find_fail_count = fail_count

    @classmethod
    def set_finds_fail_count(cls, fail_count):
        cls.finds_fail_count = fail_count

    @classmethod
    def get_find_fail_count(cls):
        return cls.find_fail_count

    @classmethod
    def get_finds_fail_count(cls):
        return cls.finds_fail_count

    @classmethod
    def reset_fail_count(cls):
        cls.finds_fail_count = 1
        cls.find_fail_count = 1
