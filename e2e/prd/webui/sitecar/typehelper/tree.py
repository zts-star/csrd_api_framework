#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:tree.py
@time:2022/11/10
"""
from csrd_api_framework.framework.e2e.prd.webui.element.helper import ElementFinder
from csrd_api_framework.framework.e2e.prd.webui.manager.webui_manager import WebUiManager


class TreeResource(object):
    def __init__(self, tree_schemas, origin_ele=None, tree_item__e2e_ele=None):
        self.tree_item_e2e_ele = tree_item__e2e_ele
        self.origin_ele = origin_ele
        self.tree_schemas = tree_schemas

    def get_expanded_ele(self):
        for tree_schema in self.tree_schemas:
            if tree_schema.schema.type == 'expanded_node':
                if self.origin_ele is not None:
                    return ElementFinder.get_element_from_element(self.origin_ele, tree_schema.schema)
                # return self.tree_item_e2e_ele.find_from_self(tree_schema)

    def get_child_tree_items(self):
        for tree_schema in self.tree_schemas:
            if tree_schema.schema.type == 'child_tree_item':
                if self.origin_ele is not None:
                    return ElementFinder.get_elements_from_element(self.origin_ele, tree_schema.schema)

    def get_can_not_expand_ele(self):
        for tree_schema in self.tree_schemas:
            if tree_schema.schema.type == 'can_not_expand_node':
                if self.origin_ele is not None:
                    return ElementFinder.get_element_from_element(self.origin_ele, tree_schema.schema)
                # return self.tree_item_e2e_ele.find_from_self(tree_schema)

    def get_could_expand_ele(self):
        for tree_schema in self.tree_schemas:
            if tree_schema.schema.type == 'could_expand_node':
                if self.origin_ele is not None:
                    return ElementFinder.get_element_from_element(self.origin_ele, tree_schema.schema)
                # return self.tree_item_e2e_ele.find_from_self(tree_schema)

    def get_items(self):
        print("get items.....")
        select_items = []
        items = {"select_items": [], "unselect_items": []}
        for tree_schema in self.tree_schemas:
            if tree_schema.schema.type == 'select_item':
                tree_schema.inits()
                select_items = tree_schema.base_elements
        for tree_schema in self.tree_schemas:
            if tree_schema.schema.type == 'item_select':
                for select_item in select_items:
                    select_item_select_ele = ElementFinder.get_element_from_element(select_item, tree_schema.schema)
                    select_item_text = select_item.get_attribute("innerText")
                    select_items_result = {"item": None, "checked": ""}
                    print(select_item_select_ele.get_attribute("checked"))
                    print(select_item_select_ele.get_attribute("type"))
                    print(select_item_select_ele.get_property("checked"))
                    print(select_item_select_ele.get_property("type"))
                    print(select_item_select_ele.get_attribute("innerHTML"))
                    checked = select_item_select_ele.get_property("checked")
                    select_items_result["item"] = select_item_text
                    select_items_result['checked'] = checked
                    print("check item....{}".format(select_items_result))
                    print("checked type..{}...{}".format(checked, type(checked)))
                    if checked is True:
                        items["select_items"].append(select_items_result)
                        # items["select_items"].append(select_item_text)
                    if checked is False:
                        items["unselect_items"].append(select_items_result)
                        # items["unselect_items"].append(select_item_text)
        print(items)
        return items


class TreeItemExpandState(object):
    def __init__(self, tree_item):
        self.tree_item = tree_item
        self.is_expanded = None
        self.could_be_expand = None
        self.not_be_expand = None
        self._initial()

    def _initial(self):
        self._check_could_expand()
        self._check_not_be_expand()
        self._check_is_expand()

    def _check_not_be_expand(self):
        try:
            self.tree_item.get_can_not_expand_ele()
            self.not_be_expand = True

        except Exception as e:
            print(e)

    def _check_could_expand(self):
        try:
            self.tree_item.get_could_expand_ele()
            self.could_be_expand = True

        except Exception as e:
            print(e)

    def _check_is_expand(self):
        try:
            self.tree_item.get_expanded_ele()
            self.is_expanded = True
        except Exception as e:
            print(e)


class TreeModel(object):
    def __init__(self, component):
        self.tree_component = component

    def get_unselect_items(self):
        tree_resource = TreeResource(self.tree_component.component_resources_elements)
        items = tree_resource.get_items()
        return items.get("unselect_items")

    def get_select_items(self):
        tree_resource = TreeResource(self.tree_component.component_resources_elements)
        items = tree_resource.get_items()
        return items.get("select_items")

    def get_items(self):
        tree_resource = TreeResource(self.tree_component.component_resources_elements)
        items = tree_resource.get_items()
        return items

    def clear_select_items(self):
        component_elements = self.tree_component.component_resources_elements
        for element in component_elements:
            if element.schema.type == 'select_all':
                element.init()
                checked = element.base_element.get_property("checked")
                if checked is True:
                    element.click()
                if checked is False:
                    element.click()
                    element.click()

    def expand_all_nodes(self):
        component_elements = self.tree_component.component_resources_elements
        for element in component_elements:
            if element.schema.type == 'tree_root':
                element.inits()
                root_elements = element.base_elements
                for root_ele in root_elements:
                    print("开始找到根节点")
                    tree_resource = TreeResource(component_elements, origin_ele=root_ele)
                    self.recur_get_tree_item(tree_resource)

    def set_item(self, item_value):
        component_elements = self.tree_component.component_resources_elements
        for element in component_elements:
            if element.schema.type == 'set_item':
                print("item ...value..{}".format(item_value))
                element.schema.parameterize(item_value)
                print("element schema parameterize...{} ".format(element.schema.locator_value))
                element.init(item_value)
                element.click()

    def recur_get_tree_item(self, tree_item):
        print("xxxxx  x111111 {}".format(tree_item))
        tree_item_stat = TreeItemExpandState(tree_item)
        print(tree_item_stat.__dict__)
        if tree_item_stat.not_be_expand:
            print("break......")
            return
        if tree_item_stat.could_be_expand:
            expand_ele = tree_item.get_could_expand_ele()
            expand_ele.click()
            import time
            time.sleep(1)
            # print(tree_item.get_attribute("innerHTML"))
            # print("xpath.....{}".format(child_tree_item))
            next_tree_items = tree_item.get_child_tree_items()
            print("next_tree_items...{}".format(next_tree_items))
            for next_tree_item in next_tree_items:
                next_tree_item_resource = TreeResource(self.tree_component.component_resources_elements,
                                                       origin_ele=next_tree_item)
                self.recur_get_tree_item(next_tree_item_resource)


if __name__ == '__main__':
    pass
