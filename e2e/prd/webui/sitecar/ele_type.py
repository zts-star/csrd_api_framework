#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:ele_type.py
@time:2022/10/20
"""

from csrd_api_framework.framework.e2e.prd.webui.element.sidecar import BaseElementTypeCommand, ElementTyeCommandSideCar
from selenium.webdriver.common.action_chains import ActionChains
from csrd_api_framework.framework.e2e.prd.webui.manager.webui_manager import WebUiManager


@ElementTyeCommandSideCar.define('input')
class InputEleTypeCommandSideCar(BaseElementTypeCommand):
    def set_value(self, args=None):
        print("xxxxxx11111111111111111111111 common input...{}".format(self.element.base_element))
        print(self.element.base_element.get_attribute("id"))
        self.element.base_element.send_keys(args)

    def get_text(self, args=None):
        print("wowc")
        print(dir(self.element.base_element))
        print(self.element.base_element.get_attribute('outerHTML'))
        print(self.element.base_element.get_attribute('value'))
        return self.element.base_element.get_attribute('value')


@ElementTyeCommandSideCar.define('dropdown_items')
class InputEleTypeCommandSideCar(BaseElementTypeCommand):
    def get_text(self, args=None):
        print("drop...woc")
        elements = self.element.base_elements
        item_values = []
        for element in elements:
            print("diao.....")
            item_value = element.get_attribute('innerText')
            item_values.append(item_value)  ##### 2022.10.19新增下拉列表文本获取表达
        return item_values


@ElementTyeCommandSideCar.define('special_input')
class InputEleTypeCommandSideCar(BaseElementTypeCommand):
    def set_value(self, args=None):
        print("xxxxxx special input...{}".format(self.element.base_element))
        print(self.element.base_element.get_attribute("id"))
        driver = WebUiManager.get_driver()
        action = ActionChains(driver)
        # time.sleep(2)
        self.element.base_element.send_keys('')
        print("argsss.....{}".format(args))
        action.move_to_element(self.element.base_element).send_keys(args).perform()


@ElementTyeCommandSideCar.define('special_input')
class InputEleTypeCommandSideCar(BaseElementTypeCommand):
    def set_value(self, args=None):
        print("xxxxxx special input...{}".format(self.element.base_element))
        print(self.element.base_element.get_attribute("id"))
        driver = WebUiManager.get_driver()
        action = ActionChains(driver)
        # time.sleep(2)
        self.element.base_element.send_keys('')
        print("argsss.....{}".format(args))
        action.move_to_element(self.element.base_element).send_keys(args).perform()


@ElementTyeCommandSideCar.define('table_head')
class InputEleTypeCommandSideCar(BaseElementTypeCommand):
    """
    获取表格头数据
    """

    def get_text(self, args=None):
        print("table_head...woc")
        elements = self.element.base_elements
        item_values = []
        for element in elements:
            print("diao table_head.....")
            item_value = element.get_attribute('innerText')
            item_values.append(item_value)  ##### 2022.10.19新增下拉列表文本获取表达
        return item_values


@ElementTyeCommandSideCar.define('table_row')
class InputEleTypeCommandSideCar(BaseElementTypeCommand):
    """
    获取表格行所有数据
    """

    def get_text(self, args=None):
        print("table_row...woc")
        elements = self.element.base_elements
        item_values = []
        for element in elements:
            print("diao table_head.....")
            item_value = element.get_attribute('innerText')
            item_values.append(item_value)  ##### 2022.10.19新增下拉列表文本获取表达
        return item_values


@ElementTyeCommandSideCar.define('text')
class TextEleTypeCommandSideCar(BaseElementTypeCommand):
    """
    新增文本类型，获取空间的文本值
    """

    def get_text(self, args=None):
        print("text ele type")
        return self.element.base_element.get_attribute('innerText')


@ElementTyeCommandSideCar.define('select_all')
class TreeSelectAllEleTypeCommandSideCar(BaseElementTypeCommand):
    """
    新增文本类型，获取空间的文本值
    """

    def click(self):
        driver = WebUiManager.get_driver()
        action = ActionChains(driver)
        action.move_to_element(self.element.base_element).click().perform()


@ElementTyeCommandSideCar.define('set_item')
class TreeSelecItemEleTypeCommandSideCar(BaseElementTypeCommand):
    """
    新增文本类型，获取空间的文本值
    """

    def click(self):
        driver = WebUiManager.get_driver()
        action = ActionChains(driver)
        action.move_to_element(self.element.base_element).click().perform()
