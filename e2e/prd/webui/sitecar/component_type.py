#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:ele_type.py
@time:2022/10/20
"""

from csrd_api_framework.framework.e2e.prd.webui.component.sidecar import BaseComponentTypeCommand, \
    ComponentTyeCommandSideCar
from csrd_api_framework.framework.e2e.prd.webui.sitecar.typehelper.tree import TreeModel
from csrd_api_framework.framework.e2e.prd.webui.element.element import Element
from selenium.webdriver.common.action_chains import ActionChains
from csrd_api_framework.framework.e2e.prd.webui.manager.webui_manager import WebUiManager


@ComponentTyeCommandSideCar.define('select_component')
class SelectComEleTypeCommandSideCar(BaseComponentTypeCommand):
    def set_value(self, args=None):
        print("开始渲染 select component。。。。。")
        component_elements = self.component.component_resources_elements
        print("需要渲染的组件元素。。。{}".format(component_elements))
        for element in component_elements:
            print("component element schema...{}".format(element.schema))
            if not element.schema.is_rendered():  # 如果这个元素是不参与渲染的，则不参加渲染操作
                print("element schema not be rendered")
                continue
            print(element.schema.type)
            element.init()
            print("select component组件元素开始赋值")
            element.click()

    def get_text(self, args=None):
        return None


@ComponentTyeCommandSideCar.define('table_component')
class SelectComEleTypeCommandSideCar(BaseComponentTypeCommand):
    """
    待完成--还未完成行元素与头元素得组合，行元素还没有完成get_text方法-2022.10.28
    """

    def get_text(self, args=None):
        print("开始get_text table_component。。。。。")
        component_elements = self.component.component_resources_elements
        print("需要获取文本的组件元素。。。{}".format(component_elements))
        table_text_result = []
        headers_values = []
        rows_values = []
        for element in component_elements:
            print("component element schema...{}".format(element.schema))
            # if not element.schema.is_rendered():  # 如果这个元素是不参与渲染的，则不参加渲染操作
            #     print("element schema not be rendered")
            #     continue
            print(element.schema.type)
            if element.schema.type == 'table_head':
                element.inits()
                base_elements = element.base_elements
                for base_element in base_elements:
                    header_value = base_element.get_attribute("innerText")
                    print('header value...{}'.format(header_value))
                    headers_values.append(header_value)
            if element.schema.type == 'table_row':
                element.inits()
                base_rows = element.base_elements
                for base_row in base_rows:
                    row_cols = base_row.get_property("children")
                    one_row_value = []
                    for row_col in row_cols:
                        row_col_value = row_col.get_attribute("innerText")
                        print("rowl col value..{}".format(row_col_value))
                        one_row_value.append(row_col_value)
                    rows_values.append(one_row_value)
        for rows_value in rows_values:
            result_item = dict(list(zip(headers_values, rows_value)))
            table_text_result.append(result_item)
        print(table_text_result)
        return table_text_result
        # ele_text = element.get_text()


@ComponentTyeCommandSideCar.define('tree_node_component')
class TreeNodeComEleTypeCommandSideCar(BaseComponentTypeCommand):
    node_level_keyword = "node_level"
    node_level_str_split = '_'

    def set_value(self, args=None):
        print("set value args.....{}".format(args))
        self._clear_all_select()
        self._expand_all_nodes()
        for arg in args:
            print("set item....")
            self._set_item(arg)

    def get_text(self, args=None):
        self._expand_all_nodes()
        tree_model = TreeModel(self.component)
        return tree_model.get_select_items()

    def _set_item(self, item):
        tree_model = TreeModel(self.component)
        tree_model.set_item(item)

    def _expand_all_nodes(self):
        """
        展开所有的节点：一级->二级->三级.....
        每个节点的类型名称规则为：node_level_1-》一级，node_level_2-》二级，node_level_3-》三级
        :return:
        """
        tree_model = TreeModel(self.component)
        tree_model.expand_all_nodes()

    def _get_ele_node_level(self, ele):
        ele_type = ele.schema.type
        if ele_type.find(self.node_level_keyword) == 0:
            level_str_s = ele_type.split(self.node_level_str_split)
            level_value = level_str_s[-1]
            return level_value

    def _clear_all_select(self):
        """
        反选所有的节点
        :return:
        """
        tree_model = TreeModel(self.component)
        tree_model.clear_select_items()


@ComponentTyeCommandSideCar.define('check_group')
class CheckGroupTypeCommandSideCar(BaseComponentTypeCommand):
    def set_value(self, args=None):
        self._clear_all_items()
        for arg in args:
            self._set_item(arg)

    def _clear_all_items(self):
        tree_model = TreeModel(self.component)
        tree_model.clear_select_items()

    def _set_item(self, item):
        tree_model = TreeModel(self.component)
        tree_model.set_item(item)

    def get_text(self, args=None):
        tree_model = TreeModel(self.component)
        return tree_model.get_select_items()
