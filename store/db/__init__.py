#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:__init__.py.py
@time:2021/09/24
"""
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import JsonSerializable
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class BaseDataObject(JsonSerializable):
    """
    这个是要把DAO返回的封装为统一的对象，比如像command对象一样
    """

    def __new__(cls, *args, **kwargs):
        logger.info("args..{},kwargs...{}".format(args, kwargs))
        obj = object.__new__(cls)
        for arg_name, arg_value in kwargs.items():
            obj.__dict__[arg_name] = arg_value
        obj.__dict__['__lines__'] = None  # 新增影响行数字段只针对修改操作
        return obj


def data_object(dao_func):
    """
    BaseDataObject对象的装饰器，该装饰器负责把返回结果转换为data_object对象
    :param dao_func:
    :return:
    """

    def wrapper(*args, **kwargs):
        logger.info(
            "data_object....args={args},kwargs={kwargs},func={func}".format(args=args, kwargs=kwargs, func=dao_func))
        result = dao_func(*args, **kwargs)
        logger.info("data object resultxxx....{}".format(result))
        results = []
        if isinstance(result, list):
            for res in result:
                results.append(BaseDataObject(**res))
            return results

        # ####2021.11.10当返回为整形时表示影响行数######
        if isinstance(result, int):
            do = BaseDataObject()
            do.__lines__ = result
            return do
        # ###########################################
        # return BaseDataObject(**result)  ##202111.10修改前的

    return wrapper


if __name__ == '__main__':
    pass
