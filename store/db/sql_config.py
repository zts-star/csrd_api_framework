#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:sql_config.py
@time:2021/09/28
"""
from csrd_api_tms.csrd_api_framework.framework import frame_config
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger

#
# class FrameworkMysqlConfig(object):
#     __src__ = "mysql"
#     port = 53306
#     host = "192.168.3.224"
#     database = "tms"
#     user = "root"
#     password = "root"


# frame_config.from_config(FrameworkMysqlConfig)
db_config = frame_config.config.get("mysql")
logger.info("db config....{}".format(db_config))

if __name__ == '__main__':
    pass
