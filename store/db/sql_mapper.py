#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:sql_mapper.py
@time:2021/09/24
"""

from csrd_api_tms.csrd_api_framework.framework.store.db.sql_session import SqlSession
from csrd_api_tms.csrd_api_framework.framework.store.db import data_object
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
import os
import traceback


class SqlMapper(object):
    sql_session = None
    sql_mapper_path = None

    @classmethod
    def mapper(cls, cls_obj):
        result = traceback.extract_stack()
        caller = result[len(result) - 2]
        file_path_of_caller = str(caller).split(',')[0].lstrip('<FrameSummary file ')
        xml_mapper_dir = os.path.dirname(file_path_of_caller)
        logger.info('调用函数的模块文件绝对路径：{}'.format(xml_mapper_dir))
        cls.sql_mapper_path = os.path.join(xml_mapper_dir, "mapper")

        def mapper_method_new(class_method):
            def wrapper(*args, **kwargs):
                logger.info("wrapper args...{} {}".format(args, kwargs))
                _mapper_method = cls.__get_mapper_method(class_method)
                sql_session_method = getattr(cls.sql_session.sql_session, _mapper_method.__name__)
                data_obj = data_object(sql_session_method)
                db_params = kwargs.get("params")
                logger.info("db params...{}".format(db_params))
                if not isinstance(db_params, dict):
                    db_params = db_params.toKeyValue()
                    kwargs["params"] = db_params
                result = data_obj(class_method, **kwargs)
                return result

            return wrapper

        cls.__get_session()
        dec_cls_methods = cls.__methods(cls_obj)
        for cls_method in dec_cls_methods:
            new_method = mapper_method_new(cls_method)
            setattr(cls_obj, cls_method, new_method)

        return cls_obj

    @classmethod
    def __get_session(cls):
        cls.sql_session = SqlSession
        SqlSession.setup(cls.sql_mapper_path)

    @classmethod
    def __get_mapper_id(cls):
        pass

    @classmethod
    def __get_mapper_method(cls, cls_method):
        print("cls_method..." + str(cls_method))
        sql_str = cls.sql_session.sql_session.mapper_dict.statement(cls_method)
        sql_str = sql_str.strip()
        if sql_str.startswith("select") or sql_str.startswith("SELECT"):
            return getattr(cls.sql_session.sql_session, "select_list")
        if sql_str.startswith("update") or sql_str.startswith("update"):
            return getattr(cls.sql_session.sql_session, "update")
        if sql_str.startswith("delete") or sql_str.startswith("DELETE"):
            return getattr(cls.sql_session.sql_session, "delete")
        if sql_str.startswith("insert") or sql_str.startswith("INSERT"):
            # return getattr(cls.sql_session.sql_session, "update")
            return getattr(cls.sql_session.sql_session, "insert")

    @classmethod
    def __methods(cls, dec_cls):
        return (list(filter(lambda m: not m.startswith("__") and not m.endswith("__") and callable(getattr(dec_cls, m)),
                            dir(dec_cls))))


class SqlMapperConfig(object):
    pass


if __name__ == '__main__':
    pass
