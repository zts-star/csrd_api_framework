#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:sql_session.py
@time:2021/09/24
"""
from py_mybatis.sql.mybatis_sql_session import MybatisMapperScanner, MybatisSqlSession, PooledDB
import pymysql
import os
from csrd_api_tms.csrd_api_framework.framework.store.db.sql_config import db_config


class SqlSession(object):
    @classmethod
    def setup(cls, mapper_path, config=None):
        path = os.path.abspath("./")
        print("path....." + path)
        print("path1....." + mapper_path)
        mapper_scanner = MybatisMapperScanner()
        # mybatis_mapper_dict = mapper_scanner.mapper_xml_scan(mapper_xml_dir=path)
        mybatis_mapper_dict = mapper_scanner.mapper_xml_scan(mapper_xml_dir=mapper_path)
        print(mybatis_mapper_dict.mapper_dict, path)
        pool = PooledDB(
            creator=pymysql,
            maxconnections=30,
            mincached=2,
            maxcached=30,
            blocking=True,
            maxusage=None,
            setsession=[],
            ping=0,
            host=db_config.get("host"),
            user=db_config.get("user"),
            password=db_config.get("password"),
            database=db_config.get("database"),
            cursorclass=pymysql.cursors.DictCursor,
            charset='utf8',
            port=db_config.get("port")
        )
        cls.sql_session = MybatisSqlSession(mapper_dict=mybatis_mapper_dict, dataSource=pool)
        cls.sql_namespace = 'Test.'


if __name__ == '__main__':
    pass
