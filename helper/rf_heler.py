#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:rf_heler.py
@time:2021/08/19
"""


def rf_ddt(keyword, *args, **kwargs):
    pass


if __name__ == '__main__':
    from robot.api.parsing import get_model, ModelVisitor


    class TestName(ModelVisitor):

        def visit_File(self, node):
            pass
            # logger.info(f"File '{node.source}' has following tests:")
            # Call `generic_visit` to visit also child nodes.
            self.generic_visit(node)

        def visit_TestCaseName(self, node):
            pass
            # logger.info(f"- {node.name} (on line {node.lineno})")
        # def visit(self, node):
        #     logger.info(node)
        #     logger.info("123")
        #     super(TestNamelogger.infoer, self).visit(node)


    # model = get_model(r'D:\csrd-work\autotest\线路障碍\报警查询\添加报警.robot')
    # logger.info(model)
    # logger.infoer = TestNamelogger.infoer()
    # logger.infoer.visit(model)

    from robot.api.parsing import get_model, ModelVisitor, Token


    class KeywordRenamer(ModelVisitor):

        def __init__(self, old_name, new_name):
            self.old_name = self.normalize(old_name)
            self.new_name = new_name

        def normalize(self, name):
            return name.lower().replace(' ', '').replace('_', '')

        def visit_KeywordName(self, node):
            '''Rename keyword definitions.'''
            if self.normalize(node.name) == self.old_name:
                token = node.get_token(Token.KEYWORD_NAME)
                token.value = self.new_name

        def visit_KeywordCall(self, node):
            # logger.info(888)
            '''Rename keyword usages.'''
            if self.normalize(node.keyword) == self.old_name:
                token = node.get_token(Token.KEYWORD)
                token.value = self.new_name

        def visit_TestCase(self, node):
            pass
            # logger.info(9999)
            # logger.info(node.body)
            #
            # '''Rename keyword definitions.'''
            # logger.info(node.name)
            # for body in node.body:
            #     logger.info(dir(body))
            #     logger.info(body.type)
            # if self.normalize(node.name) == self.old_name:
            #     token = node.get_token(Token.KEYWORD_NAME)
            #     token.value = self.new_name
            #     logger.info(token.value)


    model = get_model(r'D:\csrd-work\autotest\线路障碍\报警查询\添加报警.robot')
    renamer = KeywordRenamer('Keyword', 'New Name')
    renamer.visit(model)
    # logger.info(model)
    # logger.info(dir(model))
    # logger.info(model.sections)
    from robot.api.deco import keyword
