#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:data_util.py
@time:2021/08/20
"""
import sys
from string import Template
import json
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


def jsonModel(objectMap={}, listClassMap={}):
    """

    @jsonModel(objectMap={"car": Car}, listClassMap={"pets": Dog})
    or
    @jsonModel({"car": Car}, {"pets": Dog})
    or
    @jsonModel()
    """

    def decorate(cls):
        def fromJson(self, data):
            """ json key_value model"""
            for key in self.__dict__:
                # logger.info(("jsmodel.....", key))
                if key in data:
                    if isinstance(data[key], dict) and key in objectMap:
                        obj = self.__dict__[key] = objectMap[key]()
                        obj.fromJson(data[key])
                    elif isinstance(data[key], (list, tuple)) and key in listClassMap:
                        tempList = []
                        for item in data[key]:
                            obj = listClassMap[key]()
                            obj.fromJson(item)
                            tempList.append(obj)
                        self.__dict__[key] = tempList
                    else:
                        self.__dict__[key] = data[key]
                else:
                    pass
                    # logger.info("JsonModel log : " + key + " not in json data")

        def toKeyValue(self):
            """ model to json key_value """
            tempDic = {}
            for key in self.__dict__:
                if key in objectMap:
                    obj = self.__dict__[key]
                    tempDic[key] = obj.toKeyValue()
                elif key in listClassMap:
                    tempList = []
                    for item in self.__dict__[key]:
                        obj = item.toKeyValue()
                        tempList.append(obj)
                    tempDic[key] = tempList
                else:
                    tempDic[key] = self.__dict__[key]
            return tempDic

        @classmethod
        def objectArrayFromJsonArray(className, data):
            """create model list by json list"""
            tempList = []
            if isinstance(data, list):
                for item in data:
                    obj = className()
                    obj.fromJson(item)
                    tempList.append(obj)
            return tempList

        @classmethod
        def objectArrayToJsonArray(className, objectList):
            """dump objectList to json keyValue list"""
            tempList = []
            for obj in objectList:
                if isinstance(obj, className):
                    tempList.append(obj.toKeyValue())
            return tempList

        cls.fromJson = fromJson
        cls.toKeyValue = toKeyValue
        cls.objectArrayFromJsonArray = objectArrayFromJsonArray
        cls.objectArrayToJsonArray = objectArrayToJsonArray

        return cls

    return decorate


class TemplateModel(Template):
    delimiter = '#'

    def __new__(cls, data):
        if not hasattr(cls, 'instance'):
            cls.instance = super(TemplateModel, cls).__new__(cls)
        return cls.instance

    def __init__(self, data):
        super(TemplateModel, self).__init__(data)

    def replace_temp_data(self):
        '''#目前只支持${XXX}格式的字符串，可匹配属性、无参的方法'''
        # template = Template(data)

        _data = self.template
        try:
            match_res = self.pattern.findall(_data)
            # logger.info match_res
            if not match_res:
                return _data
            else:
                match_dict = {}

                for item in match_res:
                    att = item[2]  # 第三项表示通过${xxx}匹配到的模板字符串
                    real_att = None
                    try:
                        real_att = getattr(sys.modules[__name__], att)
                        # logger.info real_att
                        if callable(real_att):
                            match_dict[att] = real_att()
                        else:
                            match_dict[att] = real_att
                            # logger.info match_dict[att]
                    except:
                        raise

                replaced_data = self.substitute(match_dict)
                return replaced_data
        except:
            raise


class JsonCompare:
    hook_context = {}

    @classmethod
    def registry(cls, field):
        logger.info("registry...{}".format(field))

        def wrapper(func):
            logger.info(("JsonCompare registry...wrapper...", func))
            cls.hook_context.setdefault(field, func)

            def inner(*args, **kwargs):
                result = func(*args, **kwargs)
                return result

            return inner

        return wrapper

    @classmethod
    def compare(self, exp, act):
        ans = []
        self._compare(exp, act, ans, '')
        return ans

    @classmethod
    def _compare(self, a, b, ans, path):
        a = self._to_json(a)
        b = self._to_json(b)
        if type(a) != type(b):
            ans.append(f"{path} 类型不一致, 分别为{type(a)} {type(b)}")
            return
        if isinstance(a, dict):
            keys = []
            for key in a.keys():
                pt = path + "/" + key
                if key in b.keys():
                    # ##### 添加compare的hook

                    if key in self.hook_context.keys():
                        hook_func = self.hook_context.get(key)
                        b[key] = hook_func(b[key])

                    #####
                    self._compare(a[key], b[key], ans, pt)
                    keys.append(key)
                else:
                    ans.append(f"{pt} 在后者中不存在")
            for key in b.keys():
                if key not in keys:
                    pt = path + "/" + key
                    # ans.append(f"{pt} 在后者中多出")
        elif isinstance(a, list):
            i = j = 0
            while i < len(a):
                pt = path + "/" + str(i)
                if j >= len(b):
                    ans.append(f"{pt} 在后者中不存在")
                    i += 1
                    j += 1
                    continue
                self._compare(a[i], b[j], ans, pt)
                i += 1
                j += 1
            while j < len(b):
                pt = path + "/" + str(j)
                ans.append(f"{pt} 在前者中不存在")
                j += 1
        else:
            if a != b:
                ans.append(
                    f"{path} 数据不一致: {a} "
                    f"!= {b}" if path != "" else
                    f"数据不一致: {a} != {b}")

    @classmethod
    def _color(self, text, _type=0):
        if _type == 0:
            # 说明是绿色
            return """<span style="color: #13CE66">{}</span>""".format(text)
        return """<span style="color: #FF4949">{}</span>""".format(text)

    @classmethod
    def _weight(self, text):
        return """<span style="font-weight: 700">{}</span>""".format(text)

    @classmethod
    def _to_json(self, string):
        try:
            float(string)
            return string
        except:
            try:
                if isinstance(string, str):
                    return json.loads(string)
                return string
            except:
                return string


if __name__ == '__main__':
    pass
