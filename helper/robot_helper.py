#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:robot_helper.py
@time:2021/12/06
"""
from robot.libraries.BuiltIn import BuiltIn


class RobotHelper(object):
    @classmethod
    def run_keyword(cls, keyword, *args):
        return BuiltIn().run_keyword(keyword, *args)


if __name__ == '__main__':
    pass
