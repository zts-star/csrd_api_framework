# coding=utf-8
from types import MethodType
import json


class HelperAbc(object):
    """
    抽象类辅助类，如果该抽象类没有被继承并实现，那么则抛出异常
    """

    def __init__(self):
        pass

    def __new__(cls, *args, **kwargs):
        def dec_method(self, *args, **kwargs):
            raise NotImplementedError

        dec_cls = args[0]
        dec_cls_methods = cls.__methods(dec_cls)
        for method in dec_cls_methods:
            setattr(dec_cls, method, dec_method)

        return dec_cls

    @classmethod
    def __methods(cls, dec_cls):
        return (list(filter(lambda m: not m.startswith("__") and not m.endswith("__") and callable(getattr(dec_cls, m)),
                            dir(dec_cls))))


class JsonSerializable(object):

    def toDict(self):
        attr_dict = {}
        json_ser = dir(self)
        for att in json_ser:
            if not att.startswith("__"):
                value = getattr(self, att)
                if not isinstance(value, MethodType):
                    attr_dict[att] = value
        return attr_dict

    def toJson(self):
        return json.dumps(self.toDict())


import logging
import os

log_path = os.path.expanduser('~')
logger = logging.getLogger('csrd_autotest_framework')
logger.setLevel(logging.INFO)
fh = logging.FileHandler(log_path + '\csrd_autotest_framework.log', encoding='utf-8')
ch = logging.StreamHandler()
formatter = logging.Formatter(
    fmt="%(asctime)s %(levelname)s [%(name)s] [%(filename)s (%(funcName)s:%(lineno)d] - %(message)s",
    datefmt="%Y-%m-%d %X"
)
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)
