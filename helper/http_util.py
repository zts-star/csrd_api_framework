#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:http_util.py
@time:2021/08/12
"""
import requests
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class HttpUtil(object):
    @classmethod
    def get(cls, uri='', params={}, headers={}, data={}):
        res = requests.get(uri, params=params, data=data, headers=headers)
        # logger.info(res.text)
        return res

    @classmethod
    def post(self):
        pass

    @classmethod
    def put(self):
        pass

    @classmethod
    def delete(self):
        pass

    @classmethod
    def option(self):
        pass

    @classmethod
    def head(self):
        pass

    @classmethod
    def set_header(self):
        pass

    @classmethod
    def set_body(self):
        pass


if __name__ == '__main__':
    pass
