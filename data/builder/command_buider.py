#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:command_buider.py
@time:2021/09/03
"""

from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import JsonSerializable
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class Command(JsonSerializable):
    """
    输入的参数及参数值转换为Command对象的属性
    """

    def __new__(cls, *args, **kwargs):
        logger.info(args, kwargs)
        obj = object.__new__(cls)
        for arg_name, arg_value in kwargs.items():
            obj.__dict__[arg_name] = arg_value
        return obj


###因为有有些模块对command后得序列化有顺序要求，必须按照原始得dataset来，所以新增一种command来表示遵照原始顺序的Command
class CommandOrigin(object):
    def __init__(self):
        self.command = None

    def set_command(self, _command):
        self.command = _command

    def get_command(self):
        return self.command


def command(command_func):
    """
    command对象的装饰器，该装饰器负责把和输入参数转换为command对象
    :param command_func:
    :return:
    """

    def wrapper(*args, **kwargs):
        logger.debug("command....args={args},kwargs={kwargs}".format(args=args, kwargs=kwargs))
        obj = args[0]
        cmd_args = args[1]
        # 0923这里转换dataset_result为command，只取params_values的值
        cmd_args = cmd_args.dataset_params_value
        if cmd_args is None:
            cmd_args = {}
        # end
        command_obj = Command(**cmd_args)

        result = command_func(obj, command_obj)
        return result

    return wrapper


def origin_command(command_func):
    def wrapper(*args, **kwargs):
        logger.debug("command....args={args},kwargs={kwargs}".format(args=args, kwargs=kwargs))
        obj = args[0]
        cmd_args = args[1]
        # 0923这里转换dataset_result为command，只取params_values的值
        cmd_args = cmd_args.dataset_params_value
        if cmd_args is None:
            cmd_args = {}
        # end
        command_obj = CommandOrigin()
        command_obj.set_command(cmd_args)

        result = command_func(obj, command_obj)
        return result

    return wrapper


cmd = Command(a=2, b=3)
# logger.info(cmd.)
# logger.info(cmd.a)
s = """{
    "total": 11,
    "list": [
        {
            "lineId": "1a541273bb074e2280641fc96588284a",
            "lineName": "1x",
            "lineNo": "1x",
            "lineColor": "#409EFF",
            "lineType": "1",
            "lineFlag": "1",
            "lineTypeName": "地铁",
            "lineFlagName": "启用"
        },
        {
            "lineId": "2a9facf68d5242c9aaca28dd590a0574",
            "lineName": "京沪高铁",
            "lineNo": "G0002",
            "lineColor": "#409EFF",
            "lineType": "2",
            "lineStatus": "1",
            "lineFlag": "1",
            "lineTypeName": "高铁",
            "lineStatusName": "正常",
            "lineFlagName": "启用"
        },
        {
            "lineId": "3ede0aa0ad0c405f9f988adea3c26367",
            "lineName": "丹大线",
            "lineNo": "1",
            "lineColor": "#614B3D",
            "lineType": "2",
            "lineStatus": "1",
            "lineFlag": "1",
            "lineTypeName": "高铁",
            "lineStatusName": "正常",
            "lineFlagName": "启用"
        },
        {
            "lineId": "452fd9b0f36c40e182394e90961bff14",
            "lineName": "成渝高铁",
            "lineNo": "G0003",
            "lineColor": "#409EFF",
            "lineType": "2",
            "lineStatus": "1",
            "lineFlag": "1",
            "lineTypeName": "高铁",
            "lineStatusName": "正常",
            "lineFlagName": "启用"
        },
        {
            "lineId": "700c8a0d67124a2bb8203009415f1d69",
            "lineName": "3x",
            "lineNo": "3x",
            "lineColor": "#409EFF",
            "lineType": "1",
            "lineFlag": "1",
            "lineTypeName": "地铁",
            "lineFlagName": "启用"
        },
        {
            "lineId": "8482b87783564cecbbafc7185b51d046",
            "lineName": "yy",
            "lineNo": "yy",
            "lineColor": "#409EFF",
            "lineType": "1",
            "lineLength": "22",
            "lineFlag": "1",
            "lineTypeName": "地铁",
            "lineFlagName": "启用"
        },
        {
            "lineId": "976e7131108949b5951cea3db947d5be",
            "lineName": "tt1",
            "lineNo": "1122",
            "lineColor": "#409EFF",
            "lineType": "1",
            "lineLength": "667",
            "lineFlag": "1",
            "lineTypeName": "地铁",
            "lineFlagName": "启用"
        },
        {
            "lineId": "abde3609b89245a2a372358862224de8",
            "lineName": "京包线",
            "lineNo": "53",
            "lineColor": "#9A5196",
            "lineType": "2",
            "lineStatus": "1",
            "lineFlag": "1",
            "lineTypeName": "高铁",
            "lineStatusName": "正常",
            "lineFlagName": "启用"
        },
        {
            "lineId": "abe371aa9d394230812d00fa210cec27",
            "lineName": "4x",
            "lineNo": "4x",
            "lineColor": "#409EFF",
            "lineType": "1",
            "lineFlag": "1",
            "lineTypeName": "地铁",
            "lineFlagName": "启用"
        },
        {
            "lineId": "b02a92266431484fa3b51f4579fcc398",
            "lineName": "2x",
            "lineNo": "2x",
            "lineColor": "#409EFF",
            "lineType": "1",
            "lineFlag": "1",
            "lineTypeName": "地铁",
            "lineFlagName": "启用"
        }
    ],
    "pageNum": 1,
    "pageSize": 10,
    "size": 10,
    "startRow": 1,
    "endRow": 10,
    "pages": 2,
    "prePage": 0,
    "nextPage": 2,
    "isFirstPage": true,
    "isLastPage": false,
    "hasPreviousPage": false,
    "hasNextPage": true,
    "navigatePages": 8,
    "navigatepageNums": [
        1,
        2
    ],
    "navigateFirstPage": 1,
    "navigateLastPage": 2
}"""
import json

aa = json.loads(s)
print(aa)
c = Command(**aa)
print(dir(c))

if __name__ == '__main__':
    pass
