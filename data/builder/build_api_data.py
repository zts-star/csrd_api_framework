#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:build_api_data.py
@time:2021/08/20
"""

# from csrd_api_tms.csrd_api_framework.framework.api.schema.api_schema import

from csrd_api_tms.csrd_api_framework.framework.data.model.http_model import HttpModel
from csrd_api_tms.csrd_api_framework.framework.data.model.http_resp_model import HttpRespModel
import json
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode


class ApiEmiterDataBuilder(object):
    """
     构造接口真实的请求数据：
         1、通过js_provider提供与schema定义一致的测试数据
         2、测试前端发送前端测试数据过来后->最终是会落到单个接口的-》这些数据已经存在接口中字段的就不生成mock数据了，如果没有就mock数据
    """

    def __init__(self):
        pass

    def set_api(self, api):
        pass

    def set_cmd(self, cmd_dto):
        """
        从前端传入cmd，最终落入接口的dto
        :param cmd_dto:
        :return:
        """
        pass

    def get_schema(self):
        pass

    def parser_schema(self, parser):
        parser.paser()

    def convertor(self, parser):
        parser.convert()

    def build(self, api_data) -> HttpModel:
        """
        构造最终的httpmodel

        :param api_data:   api-schema对象，包含输入数据及schema定义
        :return:
        """
        logger.debug("build..api...data...{}".format(api_data))
        data = api_data.input_data
        schema = api_data.api.schema
        parser = SchemaParser(schema)

        http_model = parser.parser(schema)
        http_data = parser.convert(http_model, data)
        return http_data


class SchemaParser(object):
    """
    根据schema定义，生成httpdata
    """

    def __init__(self, schema):
        self.schema = schema

    def parser(self, schema):
        http_model = HttpModel()
        method = self.schema.method
        request_body = self.schema.req_body_other
        # request_header = self.schema.req_headers
        # print("http model headersuuuuuuuuuuuuu 111111111111111111...")
        request_header = self.schema.converted_http_model_headers
        # print("http model headersuuuuuuuuuuuuu 222222222222222222...")
        request_params = self.schema.req_params
        desc = self.schema.desc
        name = self.schema.title
        http_model.method = method
        http_model.params = request_params
        http_model.header = request_header
        # print("http model headersuuuuuuuuuuuuu..." + str(request_header))
        # print("http model headers111..." + str(self.schema.req_headers))
        # print(http_model)
        http_model.body = request_body
        http_model.name = name
        http_model.desc = desc
        http_model.uri = self.schema.path
        http_model.query = self.schema.req_query
        http_model.schema = self.schema
        # 2021.12.10req_body_type类型验证，支持form json 类型请求转换器，不然form请求的转换器会找不到
        if self.schema.req_body_type == 'form':
            http_model.body = self.schema.req_body_form
        if self.schema.req_body_type == 'json':
            http_model.body = self.schema.req_body_other
        # end
        return http_model

    def convert(self, http_model, data):

        http_model.uri = self.__format(http_model.uri, data)
        logger.info("schema....{}".format(http_model.schema))
        http_model.body = self._convert_http_mode_req_body(http_model.body, data)
        http_model.params = self._convert_http_model_req_params(http_model.params, data)
        http_model.query = self._convert_http_model_req_query(http_model.query, data)
        print("http_model headers.." + str(http_model.header))
        print("http_model body.." + str(http_model.body))
        print(http_model)
        return http_model

    def _convert_http_model_req_params(self, req_params, data):
        logger.info("req_params....{},data...{}".format(req_params, data))
        params = {}
        for param in req_params:
            name = param.get("name")
            params.setdefault(name, data.get(name))
        return params

    def _convert_http_model_req_query(self, req_query, data):
        logger.info("req_requery...{},data...{}".format(req_query, data))
        queries = {}
        for query in req_query:
            name = query.get("name")
            queries.setdefault(name, data.get(name))
        return queries

    def _convert_http_mode_req_body(self, body, data):
        logger.info("convert http model...body={body},data={data}".format(body=body, data=data))
        converter = BodyConverter().load_converter(body, data)
        return converter.convert(body)

    def __format(self, obj, data):
        """
        格式化URI
        :param obj: uri对象
        :param data: 输入数据
        :return:
        """
        try:
            format_str = obj.format(**data)
            return format_str
        except TypeError as e:
            logger.info(e)
            return ""


class BodyConverter(object):
    """
    http中的body转换器
    """

    def __init__(self):
        pass

    def load_converter(self, schema, data):
        """
        根据不同的data加载不同的转换器
        :param data:  需要转换的数据，可能是json,xml,text等，根据需要转换
        :return:
        """
        logger.info("load converter dataschema...{schema}".format(schema=schema))
        if schema is None:
            return NoneConverter(data)
        logger.info(type(schema))
        # #当schema中的properties是空字典的话，则使用noneconverter转换，以前仍然使用jsonconverter转换，会提示字段不存在的异常
        if isinstance(schema, str):
            if schema == '':
                return NoneConverter(data)
            _schema = json.loads(schema)
            properties = _schema.get("properties")
            if properties == {}:
                return NoneConverter(data)

            # end 2021.12.02

            return JsonConverter(data)
        # 2021.12.10 新增form判断和转换器
        if isinstance(schema, list):
            return FormConverter(data)


class NoneConverter(object):
    """
    当body 为None时则表示body不是json，直接传空
    """

    def __init__(self, data):
        self.data = data

    def convert(self, schema):
        logger.info("none converter..")
        # return {}
        return None


class FormConverter(object):
    """
    2021.12.10 新增form判断和转换器
    """

    def __init__(self, data
                 ):
        self.data = data

    def convert(self, schema):
        """
        只转换外层key为对应类型的默认值：字符串->""  整型->None 列表->[] object->{}
        :param schema:
        :return:
        """
        body_default = {}
        logger.info("schema...{}".format(schema))
        # schema_json = json.loads(schema)
        form_properties = schema
        if len(form_properties) == 0:
            raise FrameworkException(FrameworkExCode.YAPI_REQUEST_BODY_SCHEMA_ERROR)
        for prop_meta in form_properties:
            prop_type = prop_meta.get("type")
            prop = prop_meta.get("name")
            if prop_type == "string":
                # default = ""
                default = None
            if prop_type == "integer":
                default = None
            if prop_type == "object":
                default = {}
            if prop_type == "array":
                default = []
            if prop_type == "boolean":
                default = None
            if prop_type == 'text':
                default = None

            body_default.setdefault(prop, default)
        logger.info("form converter replace...data:{},body_default={}".format(self.data, body_default))
        lower_default_body = self._convert_default_body_lower(body_default)

        for item, value in self.data.items():
            # logger.info("data item:{};value:{}".format(item, value))
            # start 加入忽略大小写的优化 如：roleId与roleid是一样的也可以进行替换
            item_lower = item.lower()
            body_default_item = lower_default_body.get(item_lower)
            item = body_default_item

            # end

            # 如果经过convert转换后的字段在api管理器中不存在，则提示异常，不然会传入None类型的key
            if item is None:
                raise FrameworkException(FrameworkExCode.YAPI_API_REQ_FIELD_NOT_EXSIT, data=item_lower)
            # end
            body_default[item] = value
        logger.info("form converter lower_default_body={}".format(body_default))
        return body_default

    def _convert_default_body_lower(self, default_body):
        """
        建立default_body的大小写key映射
        :param default_body:
        :return:
        """
        default_body_lower = {}
        for body_item in default_body:
            default_body_lower.setdefault(body_item.lower(), body_item)
        return default_body_lower


class JsonConverter(object):
    def __init__(self, data
                 ):
        self.data = data

    def convert(self, schema):
        """
        只转换外层key为对应类型的默认值：字符串->""  整型->None 列表->[] object->{}
        :param schema:
        :return:
        """
        body_default = {}
        logger.info("schema...{}".format(schema))
        schema_json = json.loads(schema)
        properties = schema_json.get("properties")
        if properties is None:
            raise FrameworkException(FrameworkExCode.YAPI_REQUEST_BODY_SCHEMA_ERROR)
        for prop, prop_meta in properties.items():
            prop_type = prop_meta.get("type")
            if prop_type == "string":
                # default = ""
                default = None
            if prop_type == "integer":
                default = None
            if prop_type == "object":
                default = {}
            if prop_type == "array":
                default = []
            if prop_type == "boolean":
                default = None

            body_default.setdefault(prop, default)
        logger.info("replace...data:{},body_default={}".format(self.data, body_default))
        lower_default_body = self._convert_default_body_lower(body_default)

        for item, value in self.data.items():
            # logger.info("data item:{};value:{}".format(item, value))
            # start 加入忽略大小写的优化 如：roleId与roleid是一样的也可以进行替换
            item_lower = item.lower()
            body_default_item = lower_default_body.get(item_lower)
            item = body_default_item

            # end

            # 第一次修改：如果经过convert转换后的字段在api管理器中不存在，则提示异常，不然会传入None类型的key
            ### 第二次修改：去掉异常处理，不用提示异常，如果item为None，则不用赋值
            # if item is None:
            #     raise FrameworkException(FrameworkExCode.YAPI_API_REQ_FIELD_NOT_EXSIT, data=item_lower)
            # end
            # body_default[item] = value
            if item is not None:
                body_default[item] = value
        logger.info("lower_default_body={}".format(body_default))
        return body_default

    def _convert_default_body_lower(self, default_body):
        """
        建立default_body的大小写key映射
        :param default_body:
        :return:
        """
        default_body_lower = {}
        for body_item in default_body:
            default_body_lower.setdefault(body_item.lower(), body_item)
        return default_body_lower


from event_bus import EventBus

bus = EventBus()

USERS = {
    1: {
        'name': 'Ricky Bobby',
        'email': 'someuser@gmail.com',
    }
}


@bus.on('new:user')
def send_welcome_email(user_id):
    user = USERS.get(user_id)

    # Logic for sending email...
    logger.info('Sent welcome email to {}'.format(user['name']))


@bus.on('new:user')
def send_temporary_pass(user_id):
    user = USERS.get(user_id)

    # Logic for sending temp pass email...
    logger.info('Sent temp pass email to {}'.format(user['name']))


def create_user():
    # Logic for creating a user...
    user_id = 2
    bus.emit('new:user', user_id)


# create_user()
'Sent welcome email to Ricky Bobby'
'Sent temp pass email to Ricky Bobby'

if __name__ == '__main__':
    pass
