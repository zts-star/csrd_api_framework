#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:build_api_resp_data.py
@time:2021/09/07
"""
from csrd_api_tms.csrd_api_framework.framework.data.model.http_resp_model import HttpRespModel
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger

class ApiRespDataBuilder(object):
    def build(self, resp):
        return HttpRespModel(resp)


if __name__ == '__main__':
    pass
