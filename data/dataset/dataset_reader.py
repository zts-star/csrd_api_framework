#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:dataset_reader.py
@time:2021/09/02
"""
from csrd_api_tms.csrd_api_framework.framework.data.provider.auto import AutoProvider, AutoDataProvider
from csrd_api_tms.csrd_api_framework.framework.data.model.dataset import DataSet
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework import frame_config


class DatasetReader(object):
    """
    读取指定项目的数据集
    """
    # config =
    # config = {"project": "线路障碍", "address": "http://127.0.0.1:9999"}
    config = frame_config.config.get("data_meta")
    logger.info("config project@@@@@@@@@@@@@@@@@@@@@@..."+str(config))
    auto_provider = AutoProvider(config)
    dataset_src = auto_provider.get_project_config().get("dataset")
    logger.info(("dataset_src.......", dataset_src))
    auto_data_provider = AutoDataProvider(config)
    dataset_origin = None

    def load(self, dataset):
        """
        从数据源中读取指定的数据集并转换为数据集模型
        :param dataset:
        :return:
        """
        dataset_origins = self.get_dataset_origin()
        # logger.info("get dataset oringin...load func...", dataset_origins)
        datasets_load = []

        for dataset_origin in dataset_origins:
            # dataset_origin_model: DataSet = DataSet().fromJson(dataset_origin)
            dataset_origin_model: DataSet = DataSet()
            dataset_origin_model.fromJson(dataset_origin)
            # logger.info("dataset_origin_model....{}".format(dataset_origin_model.toKeyValue()))
            if dataset_origin_model.dataset_name == dataset:
                datasets_load.append(dataset_origin_model)
                # return dataset_origin_model
        return datasets_load

    def get_dataset_origin(self):
        # logger.info("dataset origin,.....", self.dataset_src)
        return self.auto_data_provider.load_data(self.dataset_src)


if __name__ == '__main__':
    pass
