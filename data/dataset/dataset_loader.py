#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:dataset_loader.py
@time:2021/09/02
"""

from csrd_api_tms.csrd_api_framework.framework.data.dataset.dataset_builder import DatasetBuilder
from csrd_api_tms.csrd_api_framework.framework.data.dataset.dataset_reader import DatasetReader
from csrd_api_tms.csrd_api_framework.framework.data.dataset.dataset_converter import DatasetConvertPreHandler
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class DatasetLoader(object):
    """
    对外部提供的数据集加载器，加载与构建指定的数据集
    """
    builder = DatasetBuilder()
    dataset_reader = DatasetReader()

    def load(self, dataset):
        """
        加载指定数据集
        :param dataset:
        :return:
        """
        return self.dataset_reader.load(dataset)

    def get_dataset_origin(self):
        """
        加载所有数据集的原始值
        :return:
        """
        return self.dataset_reader.get_dataset_origin()

    def build(self, data):
        """

        :param data:
        :return:
        """
        build_result = self.builder.build(data)
        build_result = self.builder.build_after(build_result)
        logger.info("build result....{}".format(build_result))
        return build_result


if __name__ == '__main__':
    pass
