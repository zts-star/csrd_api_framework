#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:dataset_converter.py
@time:2021/11/15
"""
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode
from csrd_api_tms.csrd_api_framework.framework.manager.framework_context import FrameworkContext
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.data.model.dataset import DataSetParams
from copy import deepcopy


################这个跟API的converter不一样，目前来说不能复用。。。。
class DatasetConvertPreHandler(object):
    """
    convert的预处理
    """

    @classmethod
    def handle(cls, dataset_result):
        dataset_params = DataSetParams(dataset_result.dataset_params)
        convertors = []
        for param in dataset_params.params_array:
            convertor = DatasetFieldConvertContext.get_converter(param)
            if convertor is not None:
                convertors.append(convertor)
        return convertors

    @classmethod
    def convert(cls, dataset_result, converters):
        for converter in converters:
            dataset_result = converter.convert(dataset_result)

        # 这里需要把dataset_result中的参数值字典pop掉以前的，新增的重新赋值
        convert_dataset_params_value = dataset_result.dataset_params_value
        for converter in converters:
            field = DatasetFieldConvertContext.get_field(converter)
            convert_dataset_params_value.pop(field)

        dataset_result.dataset_params_value = convert_dataset_params_value
        return dataset_result


class DatasetFieldConvertContext(object):
    """
    converter上下问管理-注册、获取converter
    """
    field_converters = {}

    @classmethod
    def registry_converter(cls, field):
        """
        注册转换器
        :param field:   需要转换的字段
        :return:
        """
        logger.info("dataset registry field:{}".format(field))

        def wrapper(converter_cls):
            if field not in cls.field_converters:
                cls.field_converters[field] = converter_cls()
                framework_context = deepcopy(cls.field_converters)
                FrameworkContext.dataset_field_context = framework_context  # 每个子模块的上下文都要向manager里面的context注册
            else:
                raise FrameworkException(FrameworkExCode.API_FIELD_CONVERTER_DUPLICATE)
            return converter_cls()

        return wrapper

    @classmethod
    def get_converter(cls, field):
        """
        通过字段查找转换器
        :param field: 注册的转换字段
        :return:
        """
        logger.info("dataset get_converter:{},field:{}".format(cls.field_converters, field))
        converter = cls.field_converters.get(field)
        # if converter is None:
        #     raise FrameworkException(FrameworkExCode.API_FIELD_CONVERTER_NOT_FOUND, data=field)
        return converter

    @classmethod
    def get_field(cls, converter):
        for field, _converter in cls.field_converters.items():
            if converter is _converter:
                logger.info("found the field.....{}".format(field))
                return field


if __name__ == '__main__':
    pass
