#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:dataset_helper.py
@time:2021/11/17
"""
from csrd_api_tms.csrd_api_framework.framework.data.provider.dataset_provider import DatasetProvider


class DatasetHelper(object):
    """
    数据集辅助类
    """

    @classmethod
    def dataset_value_list_convert(cls, dataset_value):
        """
        约定需要转换为列表的数据集值，默认格式有目前使用“|”进行字段分隔
        :param dataset_value:
        :return:
        """
        dataset_value_list = dataset_value.split("|")
        return dataset_value_list

    @classmethod
    def data_value_ref(cls, data_set_ref_str):
        """
        数据集参数值可以引用其他数据集
        :param data_set_ref_str:
        :return:
        """
        provider = DatasetProvider()
        dataset_result = provider.provide_one(data_set_ref_str)
        dataset_params_value = dataset_result.dataset_params_value
        return dataset_params_value


if __name__ == '__main__':
    pass
