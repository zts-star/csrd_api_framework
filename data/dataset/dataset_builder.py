#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:dataset_builder.py
@time:2021/09/02
"""
from csrd_api_tms.csrd_api_framework.framework.data.dataset.dataset_params import DatasetParamsConverter
from csrd_api_tms.csrd_api_framework.framework.data.model.dataset import DataSetParams, DataSetValues, DataSetResult
from csrd_api_tms.csrd_api_framework.framework.data.dataset.dataset_converter import DatasetConvertPreHandler
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class InternalBuildStrategy(object):
    def __init__(self, params, values):
        self.keywords = ["$ds{"]
        self.params = params
        self.values = values

    def is_build(self):
        for keyword in self.keywords:
            if self.params.origin_params.find(keyword) >= 0:
                return False
        return True

    def build(self):
        pass


class FuncParameterizeBuildStrategy(object):
    """
    目前只针对已注册的func进行值转换，针对数据集的内部引用暂不做
    """

    def __init__(self, params, values):
        self.keywords = ["${"]
        self.params = params
        self.values = values
        self.params_converter = DatasetParamsConverter()

    def is_build(self):
        for keyword in self.keywords:
            if self.params.origin_params.find(keyword) >= 0:
                return True
        return False

    def build(self):

        dataset_build_result = {}
        for index, param in enumerate(self.params.params_array):
            dataset_build_result.setdefault(param, self.params_converter.convert(self.values[index]))
        return dataset_build_result


class NotInternalBuildStrategy(object):
    """
    目前只针对已注册的func进行值转换，针对数据集的内部引用暂不做
    """

    def __init__(self, params, values):
        self.keywords = ["${", "$ds{"]
        self.params = params
        self.values = values
        self.params_converter = DatasetParamsConverter()

    def is_build(self):

        for keyword in self.keywords:
            if self.params.origin_params.find(keyword) >= 0:
                return False
        return True

    def build(self):

        dataset_build_result = {}
        for index, param in enumerate(self.params.params_array):
            logger.debug("set build result....param={param},values_array={values_array}".format(param=param,
                                                                                                values_array=
                                                                                                self.values.values_array[
                                                                                                    index]))
            dataset_build_result.setdefault(param, self.params_converter.convert(self.values.values_array[index]))
        return dataset_build_result


class BuildDatasetStrategyFactory(object):
    """
    策略工厂：获取构建数据集的策略对象
    """
    strategies = [FuncParameterizeBuildStrategy, NotInternalBuildStrategy]

    @classmethod
    def get_strategy(cls, params, values):
        for strategy in cls.strategies:
            strategy = strategy(params, values)
            if strategy.is_build():  # 判断策略是否生效，如果生效则返回该策略对象
                return strategy


class DatasetBuilder(object):
    def __init__(self):
        self.params_converter = DatasetParamsConverter()

    def build(self, dataset):
        """
        构建指定的数据集对象
        :param dataset:  需要构建的数据集名称
        :return:
        """
        params = dataset.dataset_params
        values = dataset.dataset_values
        dataset_params_obj = DataSetParams(params)
        dataset_values_obj = DataSetValues(values)
        params_array = params.split(",")
        values_array = values.split(",")
        logger.info(
            "params_array  values_array..........params_array={params_array},values_array={values_array}".format(
                values_array=values_array, params_array=params_array))
        strategy = self.allocate_strategy(dataset_params_obj, dataset_values_obj)
        strategy_build_result = strategy.build()
        # 20210923新增数据集结果对象，构建结果都是dataset_result对象，而不只是params values的字典
        dataset_result = DataSetResult()
        dataset_result.fromJson(dataset.toKeyValue())
        dataset_result.dataset_params_value = strategy_build_result
        logger.info("dataset_result.param_value......{}".format(dataset_result.toKeyValue()))
        # self._build_after(dataset_result)
        # end
        # return strategy_build_result
        return dataset_result
        # return strategy.build()

    def allocate_strategy(self, params, values):
        """
        获取数据集构建策略对象
        :param params:  DatasetParams对象
        :param values:  DatasetValues对象
        :return:
        """
        strategy = BuildDatasetStrategyFactory.get_strategy(params, values)
        return strategy

    def build_after(self, dataset_result):

        if dataset_result.dataset_type == '静态数据集' or dataset_result.dataset_type == '静态公共数据集':
            logger.info("build after")
            converters = DatasetConvertPreHandler.handle(dataset_result)
            convert_dataset_result = DatasetConvertPreHandler.convert(dataset_result, converters)
            logger.info("build after convert dataset result...{}".format(convert_dataset_result.dataset_params_value))
            return convert_dataset_result
        else:
            return dataset_result


if __name__ == '__main__':
    pass
