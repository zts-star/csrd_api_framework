#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:dataset_params.py
@time:2021/09/02
"""
from csrd_api_tms.csrd_api_framework.framework.data.provider.manager import DataProviderContext
from csrd_api_tms.csrd_api_framework.framework.data.dataset.dataset_reader import DatasetReader
import re
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class DatasetParamsConverter(object):
    def __init__(self):
        self.context = DataProviderContext()
        self.dataset_reader = DatasetReader()

    def convert(self, params):
        logger.debug("DatasetParamsConverter....{}".format(params))
        allocator = self._allocate(params)
        return allocator.get_value()

    def _allocate(self, param):
        ds_param = DatasetDsParams(param)
        func_param = DatasetFuncParams(param)
        internal_param = DatasetParamsValueInternal(param)
        no_func = DatasetNoFuncParameterize(param)
        for allocator in [func_param, no_func]:
            logger.debug("cllocate...param={param},allocator={allocator},is_correct={is_correct}".format(param=param,
                                                                                                         is_correct=allocator.type_is_correect(),
                                                                                                         allocator=allocator))
            if allocator.type_is_correect():
                logger.debug("finnal allocator...{}".format(allocator))
                allocator.get_value()
                return allocator


class DatasetDsParams(object):
    def __init__(self, param):
        self.parttern = r"(\$ds{[^${]+?})"
        self.value = None
        self.param = param
        self.dataset_reader = DatasetReader()

    def type_is_correect(self):
        pass

    def get_value(self):
        result = self.parttern.findall(self.param)
        if result:
            ds_params = result
            ds = self.dataset_reader.load(ds_params)
            return ds


class DatasetFuncParams(object):
    def __init__(self, param):
        # self.parttern = r"(\${[^${]+?})"
        # self.parttern = r"(#\[[^${]+?\])"
        self.parttern = r"(?<=#\[).*?(?=])"
        self.partter_compile = re.compile(self.parttern)
        self.value = None
        self.param = param
        self.context = DataProviderContext

    def type_is_correect(self):
        result = self.partter_compile.findall(self.param)
        # logger.info("correct...result...", result, True if result else False)
        return True if result else False

    def get_value(self):
        result = self.partter_compile.findall(self.param)
        if result:
            func_params = result[0]
            logger.info("dataset func params....param={},func_params={}".format(self.param, func_params))
            func_name_index = func_params.find("(")
            func_name = func_params[:func_name_index]
            logger.info("match func name.....{}".format(func_name))
            self.value = func_params
            # logger.info("register...self.value = func_params...", func_params)
            register_obj = self.context.get_register_obj(func_name)
            code_exec = "{func_name}=register_obj".format(func_name=func_name)
            logger.info("code....exec....{}".format(code_exec))
            exec(code_exec)
            # logger.info("eval...func_params...", func_params, register_obj)
            func_return_value = eval(func_params)
            logger.info("eval return value...{}".format(func_return_value))

            # 2021.12.09添加直接替换参数化的功能，参数函数前后都可以有值，只会替换包含指定正则的值，其他值保持原样：以前的版本整个参数会直接替换为函数返回值
            new_return_value = self._get_param_value(func_params, func_return_value)
            logger.info("new return value....{}".format(new_return_value))
            func_return_value=new_return_value
            # end
            return func_return_value

            # return register_obj()

    def _get_param_value(self, func_params, value):
        logger.info("_get_param_value....{} {}".format(func_params, value))
        try:
            replace_value = self.param.replace("#[" + func_params + "]", value)
        except TypeError:
            replace_value = value
        return replace_value


class DatasetNoFuncParameterize(object):

    def __init__(self, param):
        self.parttern = r"(#\[[^${]+?\])"
        self.partter_compile = re.compile(self.parttern)
        self.value = None
        self.param = param

    def type_is_correect(self):
        result = self.partter_compile.findall(self.param)
        # logger.info("type...correct...", False if result else True)
        return False if result else True

    def get_value(self):
        return self.param


class DatasetParamsValueInternal(object):
    def __init__(self, param):
        self.parttern = r"(\$ds{[^${]+?})"
        self.value = None
        self.param = param

    def type_is_correect(self):
        pass

    def get_value(self):
        pass


if __name__ == '__main__':
    pass
