#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:http_model.py
@time:2021/08/31
"""
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class MethodDesc(object):
    """
    Http方法描述符，侵入性小，可复用。
    """

    def __init__(self):
        self._method = None

    def __set__(self, instance, value):
        """
        http的method只能被指定为['post', 'get', 'delete', 'put', 'options', 'head']，可以区分大小写
        :param instance:
        :param value:
        :return:
        """
        methods = ['post', 'get', 'delete', 'put', 'options', 'head']
        if value.lower() not in methods:
            raise ValueError('invalid http method')

        self._method = value.lower()

    def __get__(self, instance, owner):
        return self._method

    def __delete__(self):
        del self._method


class HttpModel(object):
    """
    http协议的数据模型
    """
    method = MethodDesc()

    def __init__(self):
        self.uri = None
        self.body = None
        self.params = None
        self.headers = None
        self.address = None
        self.protocol = None
        self.resp = None
        self.name = None
        self.desc = None
        self.postfix = ""
        self.query = None
        self.schema = None



if __name__ == '__main__':
    model = HttpModel()
    model.method = "POST"
    logger.info(model.method)
