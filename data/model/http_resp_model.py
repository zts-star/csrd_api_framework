#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:http_resp_model.py
@time:2021/09/07
"""
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode
import jsonpath
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from json import JSONDecodeError


class HttpRespModel(object):
    """
    http协议的响应数据模型
    """

    def __init__(self, resp):
        self.resp = resp
        self.body = resp.text
        self.headers = resp.headers
        self.code = resp.status_code
        self.content = resp.content

    def __to_json__(self):
        content_type = "Content-Type"
        resp_model_json = {"resp": {"body": None, "headers": self.headers, "code": self.code, "content": self.content}}
        resp_key_value = resp_model_json["resp"]
        logger.debug("headers....{}".format(self.headers))
        if content_type in self.headers.keys():
            if self.headers["Content-Type"].find("application/json") >= 0:
                try:
                    resp_key_value["body"] = self.resp.json()
                except JSONDecodeError:
                    raise FrameworkException(FrameworkExCode.RESP_JSON_ERROR, data=self.body)
                return resp_model_json
            else:
                try:
                    resp_key_value["body"] = self.resp.json()
                except JSONDecodeError:
                    resp_key_value["body"] = self.body
                return resp_model_json
        resp_key_value["body"] = self.body
        logger.debug("resp key value...{}".format(resp_key_value))
        return resp_model_json

    def get(self, item):
        """
        通过jsonpath来实现，item的格式为jsonpath的格式

        :param item:  jsonpath表达式
        :return:
        """
        json_resp = self.__to_json__()
        # logger.info("json resp...{}".format(json_resp))
        result = jsonpath.jsonpath(json_resp, item)
        if result is False:
            raise FrameworkException(FrameworkExCode.JSON_PATH_ERROR_CODE, data=item)
        else:
            if len(result) == 1:
                return result[0]  # 除了报错是返回false，其他均返回的是一个list，所以当包含一个元素时就直接返回这个元素，不用list了
            else:
                return result


if __name__ == '__main__':
    pass
