#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:dataset.py
@time:2021/09/02
"""
from csrd_api_tms.csrd_api_framework.framework.helper.data_util import jsonModel
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


@jsonModel()
class DataSet(object):
    """
    数据集模型
    """
    project = None
    module = None
    func = None
    dec = None
    dataset_type = None
    dataset_name = None
    dataset_params = None
    dataset_values = None
    dataset_ext = None
    dataset_ext_item = None
    dataset_comment = None

    def __init__(self):
        self.project = None
        self.module = None
        self.func = None
        self.dec = None
        self.dataset_type = None
        self.dataset_name = None
        self.dataset_params = None
        self.dataset_values = None
        self.dataset_ext = None
        self.dataset_ext_item = None
        self.dataset_comment = None


@jsonModel()
class DataSetResult(object):
    """
    数据集模型
    """
    project = None
    module = None
    func = None
    dec = None
    dataset_type = None
    dataset_name = None
    dataset_params = None
    dataset_values = None
    dataset_ext = None
    dataset_ext_item = None
    dataset_comment = None
    dataset_params_value = None

    def __init__(self):
        self.project = None
        self.module = None
        self.func = None
        self.dec = None
        self.dataset_type = None
        self.dataset_name = None
        self.dataset_params = None
        self.dataset_values = None
        self.dataset_ext = None
        self.dataset_ext_item = None
        self.dataset_comment = None
        self.dataset_params_value = None


@jsonModel()
class DataSetParams(object):
    """
    数据集参数模型
    """
    origin_params = None
    params_array = None

    def __init__(self, origin_params):
        self.origin_params = origin_params
        self.params_array = None
        self._set_params_array()

    def _set_params_array(self):
        self.params_array = self.origin_params.split(",")


@jsonModel()
class DataSetValues(object):
    """
    数据集参数值模型
    """
    origin_values = None
    values_array = None

    def __init__(self, origin_values):
        self.origin_values = origin_values
        self.values_array = None
        self._set_values_array()

    def _set_values_array(self):
        self.values_array = self.origin_values.split(",")


if __name__ == '__main__':
    ds = DataSet()
    ds.dataset_params = "params"
    ds.dataset_values = "values"
    print(ds.toKeyValue())
    ds_result = DataSetResult()
    ds_result.fromJson(ds.toKeyValue())
    print(ds_result.toKeyValue())
