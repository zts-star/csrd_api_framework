#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc: 当前Api全局上下文
@author:zhoutishun
@file:api_context.py
@time:2021/11/09
"""


class GlobalApiContext(object):
    _req_model = None
    _resp_model = None

    @classmethod
    def set_api_req_model(cls, req):
        cls._req_model = req

    @classmethod
    def get_api_req_model(cls):
        return cls._req_model

    @classmethod
    def get_api_resp_model(cls):
        return cls._resp_model

    @classmethod
    def set_api_resp_model(cls, resp):
        cls._resp_model = resp


if __name__ == '__main__':
    pass
