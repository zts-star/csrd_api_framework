#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:base_validator.py
@time:2021/09/16
"""
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.data.provider.dataset_provider import DatasetProvider
from csrd_api_tms.csrd_api_framework.framework.data.model.dataset import DataSetResult
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode


class BaseValidatorInjector(object):
    """
    把对应的验证器注入到需要验证的方法中，该validator接受的输入参数是注入的方法返回的值
    """

    @classmethod
    def inject(self, *validator):
        """
        update-1 支持多个validator
        :param validator:
        :return:
        """
        logger.info(" validator inject...{}".format(validator))

        def wrapper(func):
            logger.info("wrapper...{}".format(func))

            def inner(*args, **kwargs):
                # 0923 新增validator的数据集
                if len(args) > 1:
                    dataset = args[1]
                    if dataset is None:
                        dataset_result = None
                    else:
                        dataset_result = DatasetProvider().provide(dataset)
                        if len(dataset_result) == 0:
                            raise FrameworkException(FrameworkExCode.NULL_DATASET_CODE)
                        dataset_result = dataset_result[0]
                else:
                    dataset_result = None
                #
                logger.info("inner...{}".format(args))
                result = func(*args, **kwargs)
                for validate in validator:
                    validate(result, dataset_result)

            return inner

        return wrapper


if __name__ == '__main__':
    pass
