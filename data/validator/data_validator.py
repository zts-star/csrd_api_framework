#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:data_validator.py
@time:2022/11/08
"""
from csrd_api_framework.framework.helper.data_util import JsonCompare
from csrd_api_tms.csrd_api_framework.framework.data.provider.dataset_provider import DatasetProvider
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode


class BaseDataValidator(object):
    @classmethod
    def contain_in_list(cls, act_results, expect_result):
        print("actual result...{}".format(act_results))
        print("expect result...{}".format(expect_result))
        is_valid = False
        compare_result = []
        if act_results is [] and expect_result:
            is_valid = False

        for act_result in act_results:
            compare_result = JsonCompare.compare(expect_result, act_result)
            if len(compare_result) == 0:
                is_valid = True
                break
        assert is_valid is True, "实际与期望不一致 {}".format(compare_result)


class BaseUiDataValidator(object):
    @classmethod
    def contain(cls, ui_actual_results, expect_dataset):
        """
        验证UI的result与静态数据集的比较
        :param ui_actual_result:
        :param expect_dataset:
        :return:
        """
        dataset_result = DatasetProvider().provide(expect_dataset)
        if len(dataset_result) == 0:
            raise FrameworkException(FrameworkExCode.NULL_DATASET_CODE)
        expect_dataset_result = dataset_result[0]
        expect_dataset_results = expect_dataset_result.dataset_params_value
        BaseDataValidator.contain_in_list(ui_actual_results, expect_dataset_results)




if __name__ == '__main__':
    pass
