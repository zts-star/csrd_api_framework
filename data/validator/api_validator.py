#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:api_validator.py
@time:2021/09/16
"""

from jsonschema import validate
import json
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode
from json import JSONDecodeError


class FrameworkApiValidator(object):
    @classmethod
    def validate_schema(cls, resp, req):
        logger.info("req.schema.res_body_is_json_schema {}".format(req.schema.res_body_is_json_schema))
        try:
            if req.schema.res_body_is_json_schema is True:
                json_body_schema = req.schema.res_body
                logger.info("json body schema..{}".format(json_body_schema))
                json_body_schema = json.loads(json_body_schema)
                resp_data = resp.get("resp.body")
                print(type(json_body_schema))
                logger.info("api.resp.schema {}".format(json_body_schema))
                logger.info("api.resp.body {}".format(resp_data))
                validate(instance=resp_data, schema=json_body_schema)
        except JSONDecodeError as e:
            raise FrameworkException(FrameworkExCode.YAPI_API_RESP_SCHEMA_ERROR,
                                     data="resp schema:{}".format(json_body_schema))


if __name__ == '__main__':
    pass
