#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:auto.py
@time:2021/08/19
"""
# from zd import ZdSdk
from functools import reduce, lru_cache
from csrd_api_tms.csrd_api_framework.framework.data.provider.zd import ZdSdk
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class AutoProvider(object):
    def __init__(self, config):
        self.zd_sdk = ZdSdk(config)
        self.project = config.get("project")
        self.user_dir = "users"
        self.auto_parent_yaml = "auto_test.yaml"
        self.auto_yaml_uri = self.user_dir + "\\" + self.auto_parent_yaml
        self._set_zd()

    def _set_zd(self):
        self.config_yaml_line_max = 1
        self.auto_test_yaml_line_max = 999

    @lru_cache()
    def get_project_config(self):
        all_auto_config = self.zd_sdk.get_data(self.auto_yaml_uri, line=self.auto_test_yaml_line_max)
        logger.info("all_auto_config.....{}".format(all_auto_config))

        for config in all_auto_config:
            logger.info(config)
            config_uri = self.user_dir + "\\" + config.get("project_schema")
            logger.info(config_uri)
            config_content = self.zd_sdk.get_data(config_uri)
            logger.info(config_content)
            if config_content:
                config_content = config_content[0]
                project = config_content.get("project")
                logger.info("project auto_test.yaml...." + str(project))
                _split = config_content.get("split")
                logger.info("cache...uri.....{}".format(config_content.get("data_schema_uri")))
                data_schema_uri = config_content.get("data_schema_uri").split(_split)
                config_content["data_schema_uri"] = data_schema_uri
                if project == self.project:
                    return config_content
            else:
                raise Exception("config content is invalid")


class AutoDataProvider(object):

    def __init__(self, config):
        self.auto_config_provider = AutoProvider(config)

    @lru_cache()
    def load_data(self, data_instance_name):
        logger.info("################################################# {}".format(data_instance_name))
        proj_config_content = self.auto_config_provider.get_project_config()
        data_schema_uri_list = proj_config_content.get("data_schema_uri")
        # logger.info("data_instance_name...", data_schema_uri_list, data_instance_name)
        logger.info("data_instance_name...{}".format(data_instance_name))
        data_schema_level = proj_config_content.get("data_schema_level")
        for uri in data_schema_uri_list:
            logger.info("load_data.....data_instance_name={data_instance_name},uri={uri}".format(
                data_instance_name=data_instance_name, uri=uri))
            yaml_uri = uri.split("\\")[-1]
            logger.info("yaml_uri...{}".format(yaml_uri))
            if data_instance_name == yaml_uri:
                logger.info("matched.....data_instance_name={data_instance_name},yaml_uri={yaml_uri}".format(
                    data_instance_name=data_instance_name, yaml_uri=yaml_uri))
                full_uri = self._get_full_uri(data_schema_level, uri)
                data = self.auto_config_provider.zd_sdk.get_data(full_uri)
                return data

    def _get_full_uri(self, data_schema_level, schema):

        fulluri = data_schema_level + "\\" + schema + ".yaml"
        logger.info("fulluri..{}".format(fulluri))
        return fulluri


# def data_provider():
#     def wrapper():
#         pass


if __name__ == '__main__':
    config = {"project": "线路障碍", "address": "http://192.168.3.224:9999"}
    # data_provider = AutoDataProvider(config)
    # logger.info(data_provider.load_data("add_alarm"))
    # from faker_schema.faker_schema import FakerSchema
    #
    # schema = {'employee_id': 'random_int', 'employee_name': 'name', 'employee address': 'address',
    #           'email_address': 'email'}
    # faker = FakerSchema()
    # data = faker.generate_fake(schema)
    # logger.info(data)
