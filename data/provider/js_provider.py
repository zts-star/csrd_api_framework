#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:js_provider.py
@time:2021/08/20
"""
import json

from pv_util import JsonSerializable
from pv_util import RequestTool
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class JsonProvider(object):
    def __init__(self):
        self.generator = None

    def gen(self, schema):
        return self.generator.gen(schema)

    def set_generator(self, generator):
        self.generator = generator


class YapiProviderGenerator(object):
    def __init__(self, config):
        self.config = config
        self.address = config.get("addresss")
        self.yapi_sdk = YapiAutoSdk(config)

    def gen(self, schema):
        return self.get_schema_data(schema)

    def get_schema_data(self, schema):
        return self.yapi_sdk.get_json_schema_data(schema)


class YapiAutoSdk(object):
    def __init__(self, config):
        self.yapi_mockjson_model = YapiMockJsonModel()
        self.schema2json_model = YaipSchema2JsonModel()
        self.config = config
        self.address = config.get("address")
        self.request = RequestTool()
        self.json_schema_path = self.address + "/api/interface/up"
        self.yapi_login_path = self.address + "/api/user/login"
        self.schema2json_path = self.address + "/api/interface/schema2json"

    def schema2json(self, schema):
        return self._schema2json(schema)

    def _send_schema(self, schema):
        self.yapi_mockjson_model.res_body = schema
        self.yapi_mockjson_model.__uri__ = self.json_schema_path
        self.request.send(self.yapi_mockjson_model)

    def _schema2json(self, schema):
        schema = json.loads(schema)
        self.schema2json_model.schema = schema
        logger.info("schema....properties,,,,", schema["properties"])
        self.schema2json_model.__uri__ = self.schema2json_path
        return self.request.send(self.schema2json_model).text

    def login(self, user):
        user.__uri__ = self.yapi_login_path
        return self.request.send(user)


class BaseRequestModel(object):
    __method__ = "get"
    __headers__ = {}
    __uri__ = ""
    __body__ = {}


class YapiMockJsonModel(BaseRequestModel, JsonSerializable):
    api_opened = False
    catid = '51'
    desc = ""
    id = "1191"
    markdown = ""
    method = "GET"
    path = "/jsongen"
    req_body_form = []
    req_body_is_json_schema = True
    req_headers = []
    req_params = []
    req_query = []
    res_body = ""
    res_body_is_json_schema = True
    res_body_type = "json"
    status = "undone"
    switch_notice = True
    tag = []
    title = "模拟json数据生成"
    __headers__ = {"Content-Type": "application/json;charset=UTF-8",
                   }
    __method__ = "post"


class YapiUserModel(BaseRequestModel, JsonSerializable):
    __headers__ = {"Content-Type": "application/json;charset=UTF-8"}
    email = "zhoutishun@csrd.cn"
    password = "123456"
    __method__ = "post"


class YaipSchema2JsonModel(BaseRequestModel, JsonSerializable):
    __method__ = "post"
    schema = {}


if __name__ == '__main__':
    logger.info(YapiMockJsonModel().toJson())
    config = {"address": "http://192.168.1.121:3000"}
    sdk = YapiAutoSdk(config)
    sdk.login(YapiUserModel())
    schema = "{\"type\":\"object\",\"properties\":{\"creationtime\":{\"type\":\"string\",\"format\":\"date-time\"},\"id\":{\"type\":\"integer\",\"format\":\"int64\"},\"lastlogintime\":{\"type\":\"string\",\"format\":\"date-time\"},\"name\":{\"type\":\"string\"},\"password\":{\"type\":\"string\"},\"passwordsalt\":{\"type\":\"string\"},\"phone\":{\"type\":\"string\"},\"roleid\":{\"type\":\"integer\",\"format\":\"int64\"},\"username\":{\"type\":\"string\"}},\"title\":\"User\",\"$$ref\":\"#/definitions/User\"}"
    sdk.schema2json(schema)
