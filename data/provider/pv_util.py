#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:pv_util.py
@time:2021/08/19
"""
import json
import yaml
import os
from types import MethodType
import requests

from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class YamlData:
    def __init__(self, file):
        if os.path.isfile(file):
            self.file = file
        else:
            raise FileNotFoundError("文件不存在")

    @property
    def data(self):
        with open(file=self.file, mode="r") as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
            return data


class JsonSerializable(object):

    def toDict(self):
        attr_dict = {}
        json_ser = dir(self)
        for att in json_ser:
            if not att.startswith("__"):
                value = getattr(self, att)
                if not isinstance(value, MethodType):
                    attr_dict[att] = value
        return attr_dict

    def toJson(self):
        return json.dumps(self.toDict())


class RequestTool(object):
    def __init__(self):
        self.session = requests.Session()

    def send(self, model):
        headers = getattr(model, "__headers__")
        method = getattr(model, "__method__")
        uri = getattr(model, "__uri__")
        body = getattr(model, "__body__")
        if body:
            data = {"url": uri, "headers": headers, "json": model.__body__}
        else:
            data = {"url": uri, "headers": headers, "json": model.toDict()}
        method_func = getattr(self.session, method)
        logger.info(data)
        res = method_func(**data)
        logger.info("oyooyo...", res.text)

        return res


if __name__ == '__main__':
    headers = {"Referer": "http://192.168.1.121:3000/project/50/interface/api/1191",
               "Cookie": "_yapi_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjQ3LCJpYXQiOjE2MzAyOTE3MTgsImV4cCI6MTYzMDg5NjUxOH0.1KjGKumzUFz-igFwdUl4mV7lYmwintmkAvjZzI8D1MU; _yapi_uid=47"}
    f = r"""{\"type\":\"object\",\"properties\":{\"creationtime\":{\"type\":\"string\",\"format\":\"date-time\"},\"id\":{\"type\":\"integer\",\"format\":\"int64\"},\"lastlogintime\":{\"type\":\"string\",\"format\":\"date-time\"},\"name\":{\"type\":\"string\"},\"password\":{\"type\":\"string\"},\"passwordsalt\":{\"type\":\"string\"},\"phone\":{\"type\":\"string\"},\"roleid\":{\"type\":\"integer\",\"format\":\"int64\"},\"username\":{\"type\":\"string\"}},\"title\":\"User\",\"$$ref\":\"#/definitions/User\"}"""
    aa = """{"req_query":[],"req_headers":[],"req_body_form":[],"title":"模拟json数据生成","catid":"51","path":"/jsongen","tag":[],"status":"undone","req_body_is_json_schema":true,"res_body_is_json_schema":true,"res_body_type":"json","res_body":"%s","switch_notice":true,"api_opened":false,"desc":"","markdown":"","method":"GET","req_params":[],"id":"1191"}""" % f
    aa_json = json.loads(aa)
    res = requests.post("http://192.168.1.121:3000/api/interface/up", headers=headers,
                        json=aa_json)
    logger.info(res.text)
