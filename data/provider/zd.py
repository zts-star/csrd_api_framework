#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:zd.py
@time:2021/08/19
"""
import requests
from functools import reduce, lru_cache
# from cacheout import Cache
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger

# cache = Cache()


class ZdProvider(object):
    def __init__(self):
        pass


class ConfigProvider(object):
    pass


class ZdSdk(object):
    def __init__(self, config):
        self.address = config.get("address")
        # self.project = config.get("project")
        self.max_line = 1000
        self.data_uri = self.address + "/data?config={}&lines={}"
        self.zd_user_dir = "usr"

    # @lru_cache()
    def get_data(self, schema, project=None, line=None):
        # logger.info("################################################")
        if project is None:
            if line is None:
                uri = self.data_uri.format(schema, self.max_line)
            if line is not None:
                uri = self.data_uri.format(schema, line)
            data = self._get_data(uri)
            return data

    # @cache.memoize()
    def _get_data(self, uri):
        # logger.info("cache..................................................{}".format(uri))
        logger.info(uri)
        res = requests.get(uri)

        res_list = res.json()
        new_res = self._remove_duplicate(res_list)
        return new_res

    def _remove_duplicate(self, src_list):
        dup_jug = lambda x, y: x if y in x else x + [y]
        new_list = (reduce(dup_jug, [[], ] + src_list))
        logger.debug("remove_duplicate...{}".format(new_list))
        return new_list


if __name__ == '__main__':
    # from functools import reduce
    pass
#
#     data_list = [
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"},
#   {"config_lms_address":"http://www.baidu.com","config_lms_mysql_conn":"jdbc:mysql://localhost:3306/test?user=root\u0026password=\u0026useUnicode=true\u0026characterEncoding=gbk\u0026autoReconnect=true\u0026failOverReadOnly=false","data_schema_level":"users\\project\\线路障碍\\bizdata\\","data_schema_uri":"add_alarm|add1_alarm","project":"线路障碍","split":"|"}
# ]
#     run_function = lambda x, y: x if y in x else x + [y]
#     logger.info(reduce(run_function, [[], ] + data_list))
#     logger.info(data_list)
#     import jsonschema
