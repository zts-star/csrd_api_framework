#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:register.py
@time:2021/09/01
"""
from csrd_api_tms.csrd_api_framework.framework.data.provider.manager import DataProviderContext
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class ExtDataProviderRegister(object):
    @classmethod
    def registry(cls, register_desc):
        logger.info("wwwwwwww")
        logger.info("registry...{register_desc}".format(register_desc=register_desc))

        def wrapper(func):
            logger.info(("registry...wrapper...", func))
            DataProviderContext.add_register(register_desc, func)

            def inner(*args, **kwargs):
                result = func(*args, **kwargs)
                return result

            return inner

        return wrapper


if __name__ == '__main__':
    pass
