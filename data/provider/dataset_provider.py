#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:dataset_provider.py
@time:2021/09/02
"""
from csrd_api_tms.csrd_api_framework.framework.data.dataset.dataset_loader import DatasetLoader
from csrd_api_tms.csrd_api_framework.framework.exception import FrameworkException, FrameworkExCode
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger
from csrd_api_tms.csrd_api_framework.framework.data.model.dataset import DataSetResult


class DatasetProvider(object):
    def __init__(self):
        self.dataset_loader = DatasetLoader()

    def provide(self, dataset):
        datasets = self.dataset_loader.load(dataset)
        logger.debug("datasets...provide...{}".format(datasets))
        result_datasets = []
        for dataset in datasets:
            logger.debug("dataset......build......", dataset.__dict__)
            data = self.dataset_loader.build(dataset)
            logger.debug("dataset_loader......build......data..{}".format(data))
            result_datasets.append(data)
        logger.debug("final result_datasets: {}".format(result_datasets))
        return result_datasets

    def provide_one(self, dataset):
        logger.info("datasets...provide...{}".format(dataset))
        return self.provide(dataset)[0]

    def _dataset2obj(self):
        pass


def dataset_provider(dataset):
    def wrapper(func):
        def inner(*args, **kwargs):
            """
            暂时只返回一个数据，多个数据需要单元测试框架配合
            :param args:
            :param kwargs:
            :return:
            """
            _provider = DatasetProvider()
            ds = _provider.provide(dataset)
            if len(ds) == 0:
                raise FrameworkException(FrameworkExCode.NULL_DATASET_CODE)
            class_obj = args[0]
            # cmd = args[1]
            ds = ds[0]  ###目前只返回第一条数据
            print("func....{}".format(func))
            result = func(class_obj, ds)
            return result

        return inner

    return wrapper


def without_params_data_provider(func):
    logger.info("without begin..{}".format(func))

    def wrapper(*args, **kwargs):
        logger.info("without_params_data_provider args....{}".format(args))
        if len(args) > 1:

            class_obj = args[0]
            dataset = args[1]
            if dataset == None:
                ds = DataSetResult()
            else:
                _provider = DatasetProvider()
                ds = _provider.provide(dataset)
                if len(ds) == 0:
                    raise FrameworkException(FrameworkExCode.NULL_DATASET_CODE)

                ds = ds[0]  ###目前只返回第一条数据
        else:
            class_obj = args[0]
            ds = DataSetResult()
        logger.info("without pramras provider dataset..classobj={class_obj},ds={ds}".format(class_obj=class_obj, ds=ds))
        print("func....{}".format(func))
        result = func(class_obj, ds)
        return result

    return wrapper


if __name__ == '__main__':
    provider = DatasetProvider()
    provider.provide("invalid.loginuser")
