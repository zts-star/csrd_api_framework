#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:manager.py
@time:2021/09/01
"""
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


class DataProviderContext(object):
    context = {}

    @classmethod
    def add_register(cls, register, register_obj):
        logger.info("add regist....")
        ex_provider_key = "ex_provider"
        register_item = {"register_name": register, "register_obj": register_obj}
        if ex_provider_key not in cls.context:
            cls.context[ex_provider_key] = [register_item]
        else:
            register_item = {"register_name": register, "register_obj": register_obj}
            cls.context[ex_provider_key].append(register_item)

    @classmethod
    def get_register_obj(cls, register):
        logger.info("context...:{}".format(cls.context.get("ex_provider")))
        for reg_item in cls.context.get("ex_provider"):
            reg_name = reg_item.get("register_name")
            if reg_name == register:
                logger.info("get register obj...{}".format(reg_item.get("register_obj")))
                return reg_item.get("register_obj")
        # return cls.context.get(register)


if __name__ == '__main__':
    pass
