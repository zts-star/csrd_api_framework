#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:__init__.py.py
@time:2021/08/12
"""
from csrd_api_framework.framework.helper.base_helper import logger


class _FrameworkConfig(object):
    config = {}

    @classmethod
    def from_config(cls, config):
        var_config = vars(config)
        logger.info("from config.....{}".format(var_config))
        # cls.config.setdefault(var_config.get("__src__"), var_config)
        cls._set_config_item(var_config)
        logger.info("call from config111....{}".format(cls.config))

    @classmethod
    def _set_config_item(cls, config):
        cls.config[config.get("__src__")] = config
        logger.info("set_config_item........")


frame_config = _FrameworkConfig
logger.info("from....")

config_context = {}
