#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
@desc:
@author:zhoutishun
@file:__init__.py.py
@time:2021/08/12
"""
from enum import unique, Enum
from csrd_api_tms.csrd_api_framework.framework.helper.base_helper import logger


@unique
class FrameworkExCode(Enum):
    NULL_DATASET_CODE = {'0': '数据集为空'}
    JSON_PATH_ERROR_CODE = {"1": "JSON格式不正确或者key不存在"}
    YAPI_REQUEST_BODY_SCHEMA_ERROR = {"2": "YAPI的请求body定义不正确"}
    API_FIELD_CONVERTER_DUPLICATE = {"3": "API的字段转换器已已存在"}
    API_FIELD_CONVERTER_NOT_FOUND = {"4": "没有找到对应的转换器，请注册转换器或检查API管理中是否存在该字段"}
    YAPI_API_SCHEMA_NOT_FOUND = {"5": "在API管理器中没有找到对应API"}
    YAPI_API_RESP_SCHEMA_ERROR = {"6": "在API管理器的响应格式不正确"}
    YAPI_API_REQ_FIELD_NOT_EXSIT = {"7": "在API管理器中不存在该字段"}
    RESP_JSON_ERROR = {"8": "响应json不正确"}

    @property
    def get_code(self):
        """
        根据枚举名称取状态码code
        :return:状态码code
        """
        return list(self.value.keys())[0]

    @property
    def get_msg(self):
        """
        根据枚举名称取状态说明message
        :return:
        """
        return list(self.value.values())[0]

    def format_msg(self, *args):
        """
        根据枚举名称取状态说明message + 原说明上增加说明文字
        :return:
        """
        return list(self.value.values())[0] + "".join(args)


class FrameworkException(BaseException):
    def __init__(self, ex_code, data=None):
        self.code = ex_code
        self.data = data

    def __str__(self):
        if self.data is None:
            msg = "异常信息--" + "异常码：" + self.code.get_code + "," + self.code.get_msg
        else:
            msg = "异常信息--" + "异常码：" + self.code.get_code + "," + self.code.get_msg + "," + self.data
        return msg


if __name__ == '__main__':
    pass
